window.Reporte = new Vue({
    el: '#reporte',

    data: {
        mk: '',
        reporte: 'operaciones'
    },

    created() {
        jQuery.ajax({
            url: '/panel/getMasterKey'
        }).done(mk => {
            this.mk = mk
        })
    },

    methods: {
        cerrarReportes() {
            Operaciones.open = false
            Cgm.open = false
            Cga.open = false
            Cta.open = false
            Log.open = false
        },

        operaciones () {
            this.cerrarReportes()

            this.reporte = 'operaciones'

            jQuery('#titulo_reporte')
                .text('Operaciones')

            // Abrir reporte de operaciones
            Operaciones.open = true

            Operaciones.buscar()
        },

        cgm() {
            this.cerrarReportes()

            this.reporte = 'cgm'

            jQuery('#titulo_reporte')
                .text('Cuenta de gastos Mexicana')

            // Abrir reporte cgm
            Cgm.open = true

            Cgm.buscar()
        },

        cga() {
            this.cerrarReportes()

            this.reporte = 'cga'

            jQuery('#titulo_reporte')
                .text('Cuenta de gastos Americana')

            // Abrir reporte cga
            Cga.open = true

            Cga.buscar()
        },

        cta() {
            this.cerrarReportes()

            this.reporte = 'cta'

            jQuery('#titulo_reporte')
                .text('Estado de cuenta')

            // Abrir reporte cta
            Cta.open = true

            Cta.buscar()
        },

        log() {
            this.cerrarReportes()

            this.reporte = 'log'

            jQuery('#titulo_reporte')
                .text('Log de usuarios')

            Log.open = true

            Log.buscar()
        }
    }
})