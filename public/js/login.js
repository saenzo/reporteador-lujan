function Login() {

	this.in = data => {
		jQuery.ajax({
			url: '/login',
			method: 'POST',
			data: data
		}).done(obj => {
			if (JSON.parse(obj) ) {
				window.location.href = '/'
			} else {
				$('#panel').effect('shake',{times: 2, distance: 5} )

				$('#logError').animate({"opacity": 1})
			}	
		}).fail(() => {
			$('#panel').effect('shake',{times: 2, distance: 5} )

			$('#logError').animate({"opacity": 1})
		})
	}
}

var _log = new Login()

$(document).on('submit', 'form', function(event) {
	event.preventDefault()
	
	var data = {
		client: $('input[name="client"]').val().trim(),
		user: $('input[name="user"]').val().trim(),
		password: $('input[name="password"]').val().trim()
	}

	_log.in(data)
})

$('#login-main').fadeIn('slow/400/fast');

window.onload = function() {
	Particles.init({
		selector: '.background',
		connectParticles: true,
		color: '#FFFAFA',
		maxParticles: 70,
		sizeVariations: 8,
		speed: 0.2
	});
};