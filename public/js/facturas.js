window.Facturas = new Vue({
    el: '#modal-facturas',

    data: {
        num_refe: ''
    },

    methods: {
        buscar() {
            this.$refs.table.getTable({
                num_refe: this.num_refe
            })
        }
    }
})

jQuery(document).on('click', 'a[href="#ver-facturas"]', function(ev) {
    ev.preventDefault()

    Facturas.num_refe = jQuery(this).attr('data-refe')

    Facturas.buscar()
})