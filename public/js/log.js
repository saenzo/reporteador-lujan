window.Log = new Vue({
    el: '#log',

    data: {
        // Iniciar con reporte abierto
        open: false,
        cargando: false,
        table: {}
    },

    methods: {
        buscar() {
            this.$refs.table.getTable()
        },

        makeXlsx() {
            this.$refs.table.makeXlsx('Log')
        }        
    }
})