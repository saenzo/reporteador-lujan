window.Datepicker = new Vue({
    el: '#datepicker',
    data: {
        fecha_inicial: '',
        fecha_final: '',
        referencia: '',
        format: 'MM/DD/YYYY',
        buscar_por: 'fecha'
    },

    created() {
        const now = new Date();
        
		const today = date.format(now, this.format)

        this.fecha_inicial = today

        this.fecha_final = today
    },

    methods: {
        // Buscar un reporte
        buscar() {
            Reporte[Reporte.reporte]()
        },

        // Descargar un reporte en excel
        descargar() {
            switch (Reporte.reporte) {
                case 'operaciones':
                    Operaciones.makeXlsx()
                    break
                case 'cgm':
                    Cgm.makeXlsx()
                    break
                case 'cga':
                    Cga.makeXlsx()
                    break
                case 'cta':
                    Cta.makeXlsx()
                    break
                case 'log':
                    Log.makeXlsx()
                    break
            }
        },

        // Obtener la fecha actual
        today() {
            const now = new Date();
        
		    return date.format(now, this.format)
        },

        date(format) {
            const now = new Date();
        
		    return date.format(now, format)
        },

        buscarPor() {
            if (this.buscar_por === 'fecha') {
                this.buscar_por = 'referencia'
            } else {
                this.buscar_por = 'fecha'
            }
        }
    }
})

jQuery(() => {	
	dp = $('.datetimepicker').datetimepicker({
        locale: 'es',
        format: Datepicker.format
    }).on("dp.change", ev => {
        const el = jQuery(ev.target).find('input')

        const fecha = 'fecha_' + el.data('fecha')

        Datepicker[fecha] = el.val()
    })
})