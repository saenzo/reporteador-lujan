(() => {
    'use strict'

    window.Admin = new Vue({
        el: '#admin',

        data: {
            mode: 'create',
            user: {
                client: '',
                user: '',
                password: '',
                other: '',
                mk: '',
                _id: '',
                admin: false
            }
        },


        mounted() {
            this.$refs.table.getTable() 
        },

        methods: {
            createUser() {
                this.mode = 'create'

                this.user = {
                    client: '',
                    user: '',
                    password: '',
                    other: '',
                    mk: '',
                    _id: '',
                    admin: false
                }

                jQuery('.modal').modal('show')
            },

            editUser(_id) {
                this.mode = 'edit'

                var user = this.$refs.table.data.rows.find(u => {
                    return u._id === _id
                })

                if (user === undefined) {
                    return
                }

                if (Array.isArray(user.client)) {
                    user.client = user.client.join()
                }

                if (Array.isArray(user.other)) {
                    user.other = user.other.join()
                }

                this.user = {
                    client: user.client,
                    user: user.user,
                    password: user.password,
                    other: user.other,
                    mk: user.mk,
                    _id: user._id
                }

                jQuery('.modal').modal('show')
            },

            deleteUser(_id) {
                jQuery.ajax({
                    url: '/admin/deleteUser',
                    method: 'POST',
                    data: {_id: _id}
                }).done(() => {
                    this.$refs.table.getTable()
                })
            },

            save() {
                var url 
                
                if (this.mode === 'create') {
                    url = '/admin/createUser'
                } else {
                    url = '/admin/editUser'
                }

                console.log(this.user)

                jQuery.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        user: JSON.stringify(this.user)
                    }
                }).done(result => {
                    console.log(result)
                    this.$refs.table.getTable()
                }).always(() => {
                    this.user = {
                        client: '',
                        user: '',
                        password: '',
                        other: '',
                        mk: '',
                        _id: '',
                        admin: false
                    }
                })
            }
        }
    })

    jQuery(document).on('click', 'a[href="#edit"]', function(ev) {
        ev.preventDefault()

        let _id = jQuery(ev.currentTarget).data('id')

        Admin.editUser(_id)
    }).on('click', 'a[href="#delete"]', function(ev) {
        ev.preventDefault()
    }).on('dblclick', 'a[href="#delete"]', function(ev) {
        ev.preventDefault()

        let _id = jQuery(ev.currentTarget).data('id')

        Admin.deleteUser(_id)
    })
})()