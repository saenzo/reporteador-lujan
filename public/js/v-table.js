(() => {
    'use strict'

    Vue.component('v-table', {
        props: [
            'field',
            'params',
            'url',
            'xlsx'
        ],
    
        data() {
            return {
                data: { },
                limit: 10,
                total: 0,
                currentPage: 1,
                loading: false,
    
                // Defaults
                align: 'left',
                sidePagination: 'user'
            }
        },

        created() {
            this.init()
        },
    
        computed: {
            // Calcular el limite de archivos por pagina
            limits() {
                var limits = []

                if (this.total > 10) {
                    limits.push(10)
                }

                if (this.total > 20) {
                    limits.push(20)
                }

                if (this.total > 50) {
                    limits.push(50)
                }

                if (this.total > 100) {
                    limits.push(100)
                }

                if (this.total > 10) {
                    limits.push(this.total)
                }

                return limits
            },
    
            // Obtener renglones ya paginados
            rows() {
                const rows = this.paginate(this.data.rows, this.limit, this.currentPage)            
                
                return rows
            },
    
            // Calcular el total de paginas
            pages() {
                const pages = Math.ceil(this.total / this.limit)

                if (this.currentPage > pages) {
                    this.currentPage = pages
                }

                return pages
            },
    
            // Calcular los titulos de la tabla
            thead() {
                const thead = []
    
                for (let field of this.field) {
                    var caret = '<i class="caret"></i>'
    
                    if (field.sort === false) {
                        caret = ''
                    }
                    
                    var width

                    if (field.title.length < 10) {
                        width = 100
                    } else {
                        width = field.title.length * 10
                    }

                    if (field.width !== undefined) {
                        width = field.width
                    } else {
                        width = width + "px"
                    }
                   
                    thead.push(`<th data-field="${field.name}" style="text-align:${field.align};width:${width};">
                        ${field.title} 
                        <span class="text-right">
                            ${caret}
                        </span>
                    </th>`)
                }
    
                return '<tr>' + thead.join('') + '<tr>'
            },
    
            // Calcular el contenido de la tabla
            tbody() {
                var rows = []
                
                for (let row of this.rows) {
                    var new_row = []
    
                    for (let field of this.field) {
                        var value = row[field.name] || ''
    
                        if (field.formatter !== undefined) {
                            value = field.formatter(row, value)
                        }
    
                        new_row.push(`<td align="${field.align}">
                            ${value}
                        </td>`)
                    }
    
                    rows.push('<tr>' + new_row.join('') + '</tr>')
                }
    
                return rows.join('')
            },

            // Calcular paginación
            pagination() {
                var current = this.currentPage,
                    last = this.pages,
                    delta = 2,
                    left = current - delta,
                    right = current + delta + 1,
                    range = []
            
                for (let i = 1; i <= last; i++) {
                    if (i == 1 || i == last || i >= left && i < right) {
                        range.push(i);
                    }
                }                

                return range
            }
        },
    
        methods: {
            init() {
                // Preparar datos por defecto
                for (let field of this.field) {
                    // Preparar formato por defecto
                    field.align = field.align || this.align
    
                    if (field.sort === undefined) {
                        field.sort = true
                    }
                }
    
                // Definir los renglones de la tabla
                if (this.data.rows === undefined) {
                    this.data.rows = []
                }
    
                // Definir el total de archivos
                if (this.data.total === undefined) {
                    this.data.total = 0
                    this.total = 0
                }

                // Definir parametro de busqueda
                if (this.params === undefined) {
                    this.params = { }
                }
            },
            
            setEvents() {
                const interval = setInterval(() => {
                    if (this.$refs.th !== undefined) {
                        clearInterval(interval)

                        this.$refs.th.firstChild.addEventListener('click', event => {
                            const field = event.target.getAttribute('data-field')
                            
                            this.sort(field)
                        })
                    }
                }, 500)
            },

            getTable(params, cb) {
                this.loading = true

                if (typeof params === 'function') {
                    cb = params
                    params = { }
                } else {
                    params = params || { }
                }

                params.limit = this.limit
                params.currentPage = this.currentPage

                jQuery.ajax({
                    url: this.url,
                    method: 'GET',
                    data: params
                }).done(data => {
                    if (typeof data === 'string') {
                        try {
                            this.data = JSON.parse(data)
                        } catch (ex) {
                            this.data = {
                                rows: [],
                                total: 0
                            }
                        }
                    }

                    this.total = this.data.total

                    this.setEvents()
                }).always(dt => {
                    this.loading = false

                    if (cb !== undefined) {
                        cb(dt)
                    }
                })
            },
    
            paginate(rows, perPage, page) {
                page = page - 1
    
                const basePage = page * perPage;
                
                return page < 0 || perPage < 1 || basePage >= rows.length ? [] 
                    : rows.slice( basePage,  basePage + perPage );
            },

            sort(o) {
                var order = 0
    
                this.data.rows.sort((a, b) => {
                    if (a[o] < b[o]) {
                        order = -1
                    } else if (a[o] > b[o]) {
                        order = 1;
                    }
                    
                    return order
                });
    
                if (order === 1) {
                    this.data.rows.reverse()
                }
            },

            // Crear titulos para el archivo xlsx
            xlsxTitles () {
                const titles = []

                for (let o of this.field) {
                    
                    if (o.xlsx === false) {
                        continue
                    }

                    var title

                    var div = document.createElement('div')

                    div.innerHTML = o.title
                    
                    title = div.textContent || div.innerText || ''

                    titles.push((''+title).trim())
                }

                return titles
            },

            // Crear renglones para el archivo xlsx
            xlsxRows() {
                const rows = []
                
                // Recorrer todo el arreglo
                for (let row of this.data.rows) {
                    const in_row = []

                    for (let o of this.field) {
                        if (o.xlsx === false) {
                            continue
                        }
                        
                        var value = ''

                        if (typeof o.formatter === 'function') {
                            var div = document.createElement('div')

                            div.innerHTML = o.formatter(row, row[o.name]) || ''
                            
                            value = div.textContent || div.innerText || ''
                        } else {
                            value = row[o.name] || ''
                        }

                        in_row.push((value+'').trim())
                    }

                    rows.push(in_row)
                }

                return rows
            },

            // Crear archivo xlsx
            makeXlsx(sheetname, ws_data) {
                console.log('crear excel')
                if (this.data.rows < 1) {
                    return false
                }

                var today_date = new Date();

                const today =  today_date.getDate() + '/'+
                    (today_date.getMonth()+1) + '/' + today_date.getFullYear();
                
                sheetname = sheetname || today
                ws_data = ws_data || []

                const wb = XLSX.utils.book_new();
                
                wb.Props = {
                    Title: sheetname,
                    Subject: sheetname,
                    Author: "slycia",
                    CreatedDate: today
                }
    
                wb.SheetNames.push(sheetname);

                ws_data.push(this.xlsxTitles())

                for (let row of this.xlsxRows()) {
                    ws_data.push(row)
                }
    
                var ws = XLSX.utils.aoa_to_sheet(ws_data);
    
                wb.Sheets[sheetname] = ws;
    
                var wbout = XLSX.write(wb, {
                    bookType: 'xlsx',
                    type: 'binary'
                });
    
                function s2ab(s) {
                    var buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
                    var view = new Uint8Array(buf); //create uint8array as viewer
                    for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
                    return buf;
                }
                
                const xlsxfilename =  '' + today_date.getDate() +
                    '' + (today_date.getMonth()+1) + 
                    '' + today_date.getFullYear() +
                    '' + today_date.getHours() + 
                    '' + today_date.getMinutes() + 
                    '' + today_date.getSeconds() + '.xlsx'

                window.saveAs(new Blob([s2ab(wbout)], {
                    type: "application/octet-stream"
                }), xlsxfilename)
            }
        },
    
        template: `<div class="row">
            <!-- Contenedor para la tabla y sus controles -->
            <div v-if="total > 0 && loading === false" class="container-fluid">
                <!--Table-->
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <thead v-html="thead" ref="th"></thead>
                        <tbody v-html="tbody"></tbody>
                    </table>
                </div>

                <!-- Controls - Row limits -->
                <div class="row" style="margin-top: 12px;">
                    <div class="col-md-6" style="margin-top: 0px;" v-show="limits.length > 0">
                        Mostrando
                        {{(currentPage * limit) - limit + 1}} a {{(currentPage * limit)}} de {{total}} filas &nbsp;
                        <div class="btn-group dropdown dropup">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                {{limit}} <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li v-for="lim in limits">
                                    <a href="#" @click.prevent="limit=lim">
                                        {{lim === total ? 'TODO' : lim}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                        Registros por pagina
                    </div>

                    <!-- Controls - Pagination -->
                    <div class="col-md-6" v-show="pagination.length > 1">
                        <ul class="pagination pull-right" style="margin-top: 0px;">
                            <li v-for="page in pagination" :class="{'active': page===currentPage}">
                                <a href="#" @click.prevent="currentPage=page" class="btn-primary">
                                    {{page}}
                                </a>
                            </li>
                        </ul>                    
                    </div>
                </div>
            </div>

            <!-- Contenedor para mostrar mensajes de la tabla -->
            <div style="margin-top: 50px;">
                <div v-if="loading">
                    <h4 class="text-center">
                        Cargando por favor espere <i class="fa fa-spin fa-spinner fa-lg"></i>
                    </h4>
                </div>
                <div v-if="!loading && total < 1">
                    <h4 class="text-center">
                        No hay registros para mostrar <i class="fa fa-search"></i>
                    </h4>
                </div>
            </div>
        </div>`
    })
})()