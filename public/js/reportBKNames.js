function Reports () {

	// variables globales
	this.type = this.xhr = this.ref = null;

	this.date_one = this.date_two = this.fn.today();

	// saber si se buscara por fecha o por referencia
	this.act = true;

	this.skip = 100;

}

// pedimento
Reports.prototype.pedimento = {

	// obtener informacion sobre una referencia
	info: function(data) {

		$.post('/pedimento/info', data, function(obj, textStatus, xhr) {

			var e = { }

			if ( obj ) {
				e = JSON.parse(obj);
			} 

			// datos generales
			$('span[name="info-ref"]').text(e.NUM_REFE || '--');
			$('span[name="info-exp-imp"]').text(e.IMP_EXPO || '');
			$('span[name="info-nombre"]').text(e.NOM_IMP || '--');
			$('span[name="info-clave"]').text(e.CVE_IMPO || '--');
			$('span[name="info-RFC"]').text(e.RFC_IMP || '--');
			$('span[name="info-calle"]').text(e.DIR_IMP || '--');
			$('span[name="info-colonia"]').text('--');
			$('span[name="info-CP"]').text(e.CP_IMP || '--');
			$('span[name="info-pais"]').text(e.PAI_INT || '--');

			// pedimento
			$('span[name="info-tipo"]').text(e.TIP_PEDI || '--');
			$('span[name="info-patente"]').text(e.PAG_AGEN || '--');
			$('span[name="info-clave-ped"]').text(e.CVE_PEDI || '--');
			$('span[name="info-peso-bruto"]').text(e.PES_BRUT || '--');
			$('span[name="info-medio-transporte-salida"]').text(e.MTR_SALI || '--');
			$('span[name="info-caja-banco"]').text(e.NUM_CAJA || '--');
			$('span[name="info-firma-pago"]').text(e.FIR_PAGO || '--');
			$('span[name="info-aduana-despacho"]').text(e.ADU_DESP || '--');
			$('span[name="info-num-ped"]').text(e.NUM_PEDI || '--');
			$('span[name="info-regimen"]').text(e.REG_ADUA || '--');
			$('span[name="info-medio-transporte-entrada"]').text(e.MTR_ENTR || '--');
			$('span[name="info-FE-validacion"]').text(e.FIR_ELEC || '--');
			$('span[name="info-num-operacion"]').text(e.NUM_OPER || '--');
			$('span[name="info-aduana-entrada"]').text(e.ADU_ENTR || '--');
			$('span[name="info-tipo-cambio"]').text(e.TIP_CAMB || '--');
			$('span[name="info-destino"]').text(e.DES_ORIG || '--');
			$('span[name="info-medio-transporte-arribo"]').text(e.MTR_ARRI || '--');
			$('span[name="info-total-vehiculos"]').text(e.TOT_VEHI || '--');
			$('span[name="info-clave-banco"]').text(e.CVE_BANC || '--');

			// fechas
			$('span[name="info-fecha-entrada"]').text(e.FEC_ENTR || '--');
			$('span[name="info-fecha-pago"]').text(e.FEC_PAGO || '--');

			// valor
			$('span[name="info-tipo-moneda"]').text(e.TIP_MOVA || '--');
			$('span[name="info-deducibles"]').text(e.TOT_DEDU || '--');
			$('span[name="info-total-efectivo"]').text(e.TOT_EFEC || '--');
			$('span[name="info-valor-dolares"]').text(e.VAL_DLLS || '--');
			$('span[name="info-valor-normal"]').text(e.VAL_NORM || '--');
			$('span[name="info-total-otro"]').text(e.TOT_OTRO || '--');
			$('span[name="info-valor-comercial"]').text(e.VAL_COME || '--');
			$('span[name="info-factor-ajuste"]').text(e.FAC_AJUS || '--');
			$('span[name="info-pago-total"]').text(e.TOT_PAGO || '--');
			$('span[name="info-incrementables"]').text(e.TOT_INCR || '--');
			$('span[name="info-DTA"]').text(e.PAR_2DTA || '--');
			$('span[name="info-factor-actualizacion"]').text(e.FAC_ACTU || '--');

			// marcas bultos y candados
			$('span[name="info-marcas"]').text(e.MAR_NUME || '--');
			$('span[name="info-candados"]').text(e.NUM_CAND || '--');
			$('span[name="info-bultos"]').text(e.CAN_BULT || '--');

		}).fail(function() {

		});		
	},

	frac: function(e) {

		$.post(base_url + 'fracciones/getFrac', e, function(obj, textStatus, xhr) {
			
			if ( obj ) {
				
				obj = JSON.parse(obj);

				$('#fracciones').html(obj.join(''));

			} else {
					
				var t = '<h2 class="text-info text-center">sin fracciones</h2>';

				$('#fracciones').html(t);
			}

		}).fail(function() {
			
			var t = '<h2 class="text-info text-center">sin fracciones</h2>';

			$('#fracciones').html(t);
		});
		
	}
};

// cuenta de gastos mexicana
Reports.prototype.cgm = {

	base_url: base_url + 'cgm/',

	aacgmere: function(e) {

		$.post(this.base_url + 'aacgmere', e, function(obj, textStatus, xhr) {

			$('li[name="loading"]').fadeOut('slow/400/fast');

			if ( ! obj) {
				$('#table-comp-mere').bootstrapTable('destroy').bootstrapTable({});

				return;
			}
			
			$('.bs-cg-comp-mere').modal('show');

			obj = JSON.parse(obj);

			$('#table-comp-mere').bootstrapTable('destroy').bootstrapTable({
	            columns: obj.columns,
	            data: obj.rows
	        });

		}).fail(function() {

			$('li[name="loading"]').fadeOut('slow/400/fast');

			$('#table-comp-mere').bootstrapTable('destroy').bootstrapTable({});

		});
	},

	aacgcomp: function (e) {

		$.post(this.base_url + 'aacgcomp', e, function(obj, textStatus, xhr) {

			$('li[name="loading"]').fadeOut('slow/400/fast');

			if ( ! obj) {
				$('#table-comp-mere').bootstrapTable('destroy').bootstrapTable({});

				return;
			}
			
			$('.bs-cg-comp-mere').modal('show');

			obj = JSON.parse(obj);

			$('#table-comp-mere').bootstrapTable('destroy').bootstrapTable({
	            columns: obj.columns,
	            data: obj.rows
	        });

		}).fail(function() {

			$('li[name="loading"]').fadeOut('slow/400/fast');

			$('#table-comp-mere').bootstrapTable('destroy').bootstrapTable({});

		});
	}
};

// configuraciones
Reports.prototype.cfg = {
	base_url: base_url
};

// extra funciones
Reports.prototype.fn = {

	// obtener fecha al dia
	today: function() {

		var today = new Date();
	    var dd = today.getDate();
	    var mm = today.getMonth() + 1;

	    var yyyy = today.getFullYear();

	    if ( dd < 10 ) {

	        dd = '0' + dd;
	    }

	    if ( mm < 10 ) {

	        mm = '0' + mm;
	    
	    }

	    return mm + '/' + dd + '/' + yyyy;
	},

	// validar fechas
	checkDate: function() {

		_rep.date_one = $('input[name="date-one"]').val();

		_rep.date_two = $('input[name="date-two"]').val();

		// verificar fecha uno
		if ( _rep.date_one == null || _rep.date_one == false || _rep.date_one == '' ) {
			_rep.date_one = this.today();
		}

		// verificar fecha dos
		if ( _rep.date_two == null || _rep.date_two == false || _rep.date_two == '' ) {
			_rep.date_two = this.today();
		}


		// mostrar fecha de busqueda
		$('span[name="date-one"]').text(_rep.date_one);
		$('span[name="date-two"]').text(_rep.date_two);

		// mostrar fechas
		$('input[name="date-one"]').val(_rep.date_one);
		$('input[name="date-two"]').val(_rep.date_two);
	
	}
};

var _rep = new Reports();

// al iniciar el documento
$(document).ready(function() {
	
	// cargar formato para los calendrios | seleccion de fechas
	$('.datepicker').datepicker();

	// hacer click en el reporte de pedimentos | cargar reporte
	$('a[href="#ped"][name="report"]').click();

});


// al cerrar ventana de seleccion de fechas
jQuery(document).on('click', 'button[name="find-by-date"]', function(event) {
	// Mostrar tabla de operaciones
	Operaciones.show = true

	jQuery('a[href="#operaciones"][name="report"]').click();
});

// descargar un reporte en forma excel
$(document).on('click', 'a[href="#to-excel"]', function(ev) {
	ev.preventDefault();

	Operaciones.makeXlsx()
});

// pedimentos
jQuery(document).on('click', 'a[href="#operaciones"]', function(event) {
	event.preventDefault();

	// Abortar operacióin en curso
	if (_rep.xhr !== null) {
		_rep.xhr.abort();
	}

	// Mostrar tabla de operaciones
	Operaciones.show = true

	// almacenar el tipo de reporte | ped = pedimento
	_rep.type = 'operaciones';

	// busqueda por rango de fechas
	_rep.act = true;

	// almacenar el nombre del reporte
	var rep = 'OPERACIONES';

	// mostrar el tipo de reporte
	$('#main-table-title').text(rep);

	// mostrar reporte seleccionado
	$('span[name="report-name"]').text(rep).parent('li').fadeIn('slow/400/fast');

	// validar fechas
	_rep.fn.checkDate();

	Operaciones.find(_rep.date_one, _rep.date_two)
});


// cuenta de gastos mexicana
$(document).on('click', 'a[href="#cgm"]', function(event) {
	event.preventDefault();

	// Esconder tabla de operaciones
	Operaciones.show = false

	// almacenar el tipo de reporte | cgm = cuenta gastos mexicana
	_rep.type = 'cgm';

	// busqueda por rango de fechas
	_rep.act = true;

	// almacenar el nombre del reporte
	var rep = this.text.trim().toUpperCase();

	// mostrar el tipo de reporte
	$('#main-table-title').text(rep);

	// mostrar reporte seleccionado
	$('span[name="report-name"]').text(rep).parent('li').fadeIn('slow/400/fast');

	// validar fechas
	_rep.fn.checkDate();

	// obtener tabla
	_rep.getTable.bydate({
		date_one: _rep.date_one,
		date_two: _rep.date_two
	}, 'cgm');

	// mostrar mensaje de cargando
	$('li[name="loading"]').fadeIn('slow/400/fast');


// buscar cuenta de gastos por numero de cuenta o trafico | ventana modal
}).on('click', 'button[name="find-cgm"]', function(event) {
	event.preventDefault();
	
	// almacenar el tipo de reporte | cgm = cuenta gastos mexicana
	_rep.type = 'cgm';

	_rep.ref = $('input[name="find-cgm"]:text').val().trim();	

	// no hacer nada si el campo esta vacio
	if ( _rep.ref.length <= 0 ) return;

	// busqueda por una referencia
	_rep.act = false;

	$('li[name="loading"]').fadeIn('slow/400/fast');

	// realizar consulta
	_rep.getTable.byref({ref: _rep.ref}, 'cgm');
	

// mostrar tabla aacgmere
}).on('click', 'button[name="cgm-aacgmere"]', function(event) {	
	event.preventDefault();

	// mostrar mensaje de cargando 
	$('li[name="loading"]').fadeIn('slow/400/fast');

	var no_serie = this.getAttribute('no_serie').trim() || false;

	var no_ctagas = this.getAttribute('no_ctagas').trim() || false;

	_rep.cgm.aacgmere({
		no_serie: no_serie,
		no_ctagas: no_ctagas
	});


// mostrar tabla aacgcomp
}).on('click', 'button[name="cgm-aacgcomp"]', function(event) {	
	event.preventDefault();

	// mostrar mensaje de cargando 
	$('li[name="loading"]').fadeIn('slow/400/fast');

	var no_serie = this.getAttribute('no_serie').trim() || false;

	var no_ctagas = this.getAttribute('no_ctagas').trim() || false;

	_rep.cgm.aacgcomp({
		no_serie: no_serie,
		no_ctagas: no_ctagas
	});

});

// cuenta de gastos americana
$(document).on('click', 'a[href="#cga"]', function(event) {
	event.preventDefault();

	// Esconder tabla de operaciones
	Operaciones.show = false

	// almacenar el tipo de reporte | cga = cuenta gastos americana
	_rep.type = 'cga';

	// busqueda por rango de fechas
	_rep.act = true;

	// almacenar el nombre del reporte
	var rep = this.text.trim().toUpperCase();

	// mostrar el tipo de reporte
	$('#main-table-title').text(rep);

	// mostrar reporte seleccionado
	$('span[name="report-name"]').text(rep).parent('li').fadeIn('slow/400/fast');

	// validar fechas
	_rep.fn.checkDate();

	// obtener tabla
	_rep.getTable.bydate({
		date_one: _rep.date_one,
		date_two: _rep.date_two
	}, 'cga');

	// mostrar mensaje de cargando
	$('li[name="loading"]').fadeIn('slow/400/fast');


// buscar cuenta de gastos por numero de cuenta o trafico | ventana modal
}).on('click', 'button[name="find-cga"]', function(event) {
	event.preventDefault();
	
	// almacenar el tipo de reporte | cga = cuenta gastos mexicana
	_rep.type = 'cga';

	_rep.ref = $('input[name="find-cga"]:text').val().trim();	

	// no hacer nada si el campo esta vacio
	if ( _rep.ref.length <= 0 ) return;

	// busqueda por una referencia
	_rep.act = false;

	$('li[name="loading"]').fadeIn('slow/400/fast');

	// realizar consulta
	_rep.getTable.byref({ref: _rep.ref}, 'cga');

// Log de usuarios
}).on('click', 'a[href="#log"][name="log"]', function(event) {
	event.preventDefault();

	$('li[name="loading"]').fadeIn('slow/400/fast');

	$.get("/panel/getLog", function(obj, textStatus, xhr) {

			$('li[name="loading"]').fadeOut('slow/400/fast');

			if ( ! obj) {
				$('#table-logs').bootstrapTable('destroy').bootstrapTable({});

				return;
			}
			
			$(".bs-log").modal("show");

			obj = JSON.parse(obj);

			$('#table-logs').bootstrapTable('destroy').bootstrapTable({
	            columns: obj.columns,
	            data: obj.rows
	        });

		}).fail(function() {

			$('li[name="loading"]').fadeOut('slow/400/fast');

			$('#table-logs').bootstrapTable('destroy').bootstrapTable({});

			$(".bs-log").modal("show");

		});
});
