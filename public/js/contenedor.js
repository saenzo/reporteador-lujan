window.Contenedor = new Vue({
    el: '#modal-contenedor',

    data: {
        num_refe: ''
    },

    methods: {
        buscar() {
            this.$refs.table.getTable({
                num_refe: this.num_refe
            })
        }
    }
})

jQuery(document).on('click', 'a[href="#ver-contenedor"]', function(ev) {
    ev.preventDefault()

    Contenedor.num_refe = jQuery(this).attr('data-refe')
    
    Contenedor.buscar()
})