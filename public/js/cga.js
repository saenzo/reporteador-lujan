window.Cga = new Vue({
    el: '#cga',

    data: {
        open: false
    },

    methods: {
        buscar() {
            // Buscar por fecha
            if (Datepicker.buscar_por === 'fecha') {
                this.$refs.table.getTable({
                    date_one: Datepicker.fecha_inicial,
                    date_two: Datepicker.fecha_final
                })
                
            // Buscar por referencia
            } else {
                this.$refs.table.getTable({
                    num_refe: Datepicker.referencia,
                })
            }
        },

        makeXlsx() {
            this.$refs.table.makeXlsx('Cga')
        }
    }
})
