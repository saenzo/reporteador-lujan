window.Operaciones = new Vue({
    el: '#operaciones',

    data: {
        // Iniciar con reporte abierto
        open: true
    },

    mounted() {        
        this.buscar()
    },

    methods: {
        buscar() {
            // Buscar por fecha
            if (Datepicker.buscar_por === 'fecha') {
                this.$refs.table.getTable({
                    date_one: Datepicker.fecha_inicial,
                    date_two: Datepicker.fecha_final
                })
                
            // Buscar por referencia
            } else {
                this.$refs.table.getTable({
                    num_refe: Datepicker.referencia,
                })
            }
            
        },

        makeXlsx() {            
            const clients = []

            const rows = this.$refs.table.data.rows

            for (let o of rows) {
                if (!clients.includes(' ' + o.CVE_IMPO)) {
                    clients.push(' ' + o.CVE_IMPO )
                }
            }

            const date_today = this.obtenerFecha2(Datepicker.today(), '/')
            const fecha_inicial = this.obtenerFecha2(Datepicker.fecha_inicial, '/')
            const fecha_final = this.obtenerFecha2(Datepicker.fecha_final, '/')

            var ws_data = [
                ['SERGIO LUJAN Y CIA. S.C.'],
                ['Pedimentos y partidas'],
                ['Elaborado el ' + date_today],
                ['Solo Importación'],
                ['Fecha Inicial: ' +
                    fecha_inicial + ' Fecha Final: ' + fecha_final],
                ['Clientes:' + clients.toString()],
                ['']
            ]


            this.$refs.table.makeXlsx('Operaciones', ws_data)
        },

        obtenerFecha(o, format) {
            format = format || '-'

            if (o) {
                var date = o.split(' ')[0]

                date = date.split(format)
                
                return date[2] + format + date [1] + format + date[0]
            } else {
                return ''
            }
        },

        obtenerFecha2(o, format) {
            format = format || '-'

            if (o) {
                var date = o.split(' ')[0]

                date = date.split(format)
                
                return date[1] + format + date [0] + format + date[2]
            } else {
                return ''
            }
        }
    }
})