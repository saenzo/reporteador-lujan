<script src="/public/js/ie10-viewport-bug-workaround.js"></script>

<?php // variables globales para JS ?>
<script>
    // opciones para los tooltip
    var tooltipster_ops = {
        animation: 'fade',
        delay: 200,
        theme: 'tooltipster-shadow',
        touchDevices: false,
        trigger: 'hover',
        multiple: true,
        contentAsHTML: true
    };
</script>

<?php // componentes del sistema ?>
<script src="/node_modules/vue/dist/vue.min.js"></script>
<script src="/node_modules/jquery/dist/jquery.min.js"></script>
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
<script src="/bower_components/moment/min/moment-with-locales.min.js"></script>
<script src="/node_modules/bootstrap/js/transition.js"></script>
<script src="/node_modules/bootstrap/js/collapse.js"></script>
<script src="/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script src="/node_modules/bootstrap-table/dist/bootstrap-table.min.js"></script>
<script src="/node_modules/bootstrap-table/dist/locale/bootstrap-table-es-MX.min.js"></script>

<script src="/node_modules/tooltipster/dist/js/tooltipster.bundle.min.js"></script>

<script src="/node_modules/xlsx/dist/xlsx.full.min.js"></script>
<script src="/node_modules/file-saver/dist/FileSaver.min.js"></script>

<script src="/node_modules/date-and-time/date-and-time.min.js"></script>

<script src="/public/js/v-table.js"></script>

<?php // El selector de fechas debe de ir primero ?>
<script src="/public/js/reporte.js"></script>

<script src="/public/js/datepicker.js"></script>

<script src="/public/js/operaciones.js"></script>

<script src="/public/js/contenedor.js"></script>

<script src="/public/js/facturas.js"></script>

<script src="/public/js/cgm.js"></script>

<script src="/public/js/cga.js"></script>

<script src="/public/js/cta.js"></script>

<script src="/public/js/log.js"></script>

<?php // acciones a realizar al cargar el DOM ?>
<script>
    // Create our number formatter.
    var currency = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    });

    jQuery(() => {
        jQuery('.tooltipster').tooltipster(tooltipster_ops);

        jQuery('.tooltipster-top').tooltipster(
            $.extend(tooltipster_ops, {position: 'top'})
        );

        jQuery('.tooltipster-right').tooltipster(
            $.extend(tooltipster_ops, {position: 'right'})
        );

        jQuery('.tooltipster-bottom').tooltipster(
            $.extend(tooltipster_ops, {position: 'bottom'})
        );

        jQuery('.tooltipster-left').tooltipster(
            $.extend(tooltipster_ops, {position: 'left'})
        );

        setTimeout(() => {
            jQuery('header, body').show()
        }, 500);
    })

    jQuery(document).on('all.bs.table', 'table', function() {
        jQuery('table .tooltipster').tooltipster(tooltipster_ops);
    });
</script>
