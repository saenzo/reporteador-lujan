<div style="margin: 6px;"></div>
<div class="container-fluid">
    <div class="pull-left">
        Mostrando
        {{(table.currentPage * table.limit) - table.limit + 1}} a
        {{(table.currentPage * table.limit)}} de {{table.tableRows.length}} filas &nbsp;
        <div class="btn-group dropdown dropup">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                {{table.limit}}
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#" @click.prevent="table.limit=10">10</a></li>
                <li><a href="#" @click.prevent="table.limit=25">25</a></li>
                <li><a href="#" @click.prevent="table.limit=50">50</a></li>
                <li><a href="#" @click.prevent="table.limit=100">100</a></li>
                <li>
                    <a href="#" @click.prevent="table.limit=table.tableRows.length" :title="table.tableRows.length">
                        TODO
                    </a>
                </li>
            </ul>
        </div>
        Registros por pagina
    </div>
    <div class="pull-right" style="margin-top: -18px;">
        <ul class="pagination " v-if="table.pages > 1">
            <li v-for="page in table.pagination(table.currentPage)"
                :class="{'active': page===table.currentPage}">
                <a href="#" @click.prevent="table.currentPage=page" class="btn-primary">
                    {{page}}
                </a>
            </li>
        </ul>
    </div>
</div>
