<!-- Modal -->
<div id="modal-contenedor" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
                <h4 class="modal-title">
                    Contenedor - {{num_refe}}
                </h4>
            </div>
            <div class="modal-body">
                <v-table
                    ref="table"
                    url="/contenedor"
                    :field="[{
                            name: 'NUM_CONT',
                            title: 'Numero de contenedor',
                            width: '200px'
                        },{
                            name: 'CVE_CONT',
                            title: 'Clave de contenedor',
                            width: '180px'
                        },{
                            name: 'NUM_SELLOS',
                            title: 'Sellos',
                            width: '100px'
                        }
                    ]"
                />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cerrar
                </button>
            </div>
        </div>

    </div>
</div>