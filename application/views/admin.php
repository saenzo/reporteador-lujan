<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <?php $this->load->view('/other/css') ?>
</head>
<body>
    <div style="margin-top: 16px;"></div>
    <div id="admin" class="container-fluid">
        <button type="button" class="btn btn-primary" @click="createUser()">
            Crear un usuario
        </button>    
        <div style="margin-top: 16px;"></div>
        <div class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">
                            <span v-if="mode==='create'">
                                Crear un usuario
                            </span>
                            <span v-else>
                                Editar usuario
                            </span>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <strong>
                            Nombre de cliente:
                        </strong>
                        <input type="text" class="form-control" v-model="user.client">
                        <strong>
                            Nombre de usuario:
                        </strong>
                        <input type="text" class="form-control" v-model="user.user">
                        <strong>
                            Clave de cliente:
                        </strong>
                        <small class="text-muted">
                            separar claves con una coma. ejemplo - [ cve01, cve02, cve03 ]
                        </small>
                        <input type="text" class="form-control" v-model="user.other">
                        <strong>
                            Contraseña:
                        </strong>
                        <input type="text" class="form-control" v-model="user.password">
                        <strong>
                            Clave classifile:
                        </strong>
                        <small class="text-muted">
                            Clave generada en classifile
                        </small>
                        <input type="text" class="form-control" v-model="user.mk">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" @click="save">
                            Guardar
                        </button>
                    </div>
                </div>

            </div>
        </div>
        <v-table ref="table" url="/admin/getUsers" :field="[
                {
                    name: 'client',
                    title: 'Nombre cliente'
                },
                {
                    name: 'user',
                    title: 'Usuario'
                },
                {
                    name: 'password',
                    title: 'Contraseña'
                },
                {
                    name: 'other',
                    title: 'Clave cliente',
                    formatter(row) {
                        if (row.other === true) {
                            return ''
                        } else {
                            return row.other
                        }
                    }
                },
                {
                    name: 'mk',
                    title: 'Clave classifile'
                },
                {
                    title: 'Eliminar',
                    width: '50px',
                    align: 'center',
                    formatter(row) {
                        if (row.other === true) {
                            return ''
                        }

                        return `<a href='#delete' data-id='${row._id}'>
                            <strong>&times;</strong>
                        </a>`
                    }
                },
                {
                    title: 'Editar',
                    width: '50px',
                    align: 'center',
                    formatter(row) {
                        if (row.other === true) {
                            return ''
                        }

                        return `<a href='#edit' data-id='${row._id}'>
                            <strong>&equiv;</strong>
                        </a>`
                    }
                }
            ]"
        ></v-table>
    </div>
</body>
<script src="/node_modules/jquery/dist/jquery.min.js"></script>
<script src="/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/node_modules/vue/dist/vue.min.js"></script>
<script src="/public/js/v-table.js"></script>
<script src="/public/js/admin.js"></script>
</html>