<?php echo doctype(); ?>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv='expires' content='-1' />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Sergio Lujan y CIA</title>

	<?php $this->load->view('/other/css') ?>
</head>

<header style="display: none;">
	<?php $this->load->view('header'); ?>
</header>

<body style="margin-top: 90px; display: none;">
	<?php $this->load->view('body'); ?>
</body>

<?php $this->load->view('/other/js') ?>

</html>