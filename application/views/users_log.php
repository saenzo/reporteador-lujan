<v-table
    v-show="open"
    ref="table"
    url="/panel/getLog"
    :field="[
        {
            name: 'client',
            title: 'Cliente',
            sort: false
        },{
            name: 'user',
            title: 'Usuario'
        },{
            name: 'browser',
            title: 'Navegador'
        },{
            name: 'ip',
            title: 'IP'
        },{
            name: 'date',
            title: 'Fecha'
        }
    ]"
/>