<v-table
    v-show="open"
    ref="table"
    url="/cta"
    :field="[
    {
        name: 'tipo_mov',
        title: 'Tipo'
    },
    {
        name: 'no_banco',
        title: 'Banco'
    },
    {
        name: 'no_mov',
        title: 'Movimiento'
    },
    {
        name: 'trafico',
        title: 'Trafico'
    },
    {
        name: 'fecha',
        title: 'Fecha',
        formatter: function(row) {
            if (row.saldo || row.saldo_total || !row.fecha) {
                return ''
            }

            var fecha = row.fecha.replace('00:00:00.000','')

            fecha = fecha.split('-')

            return fecha[2] + '-' + fecha[1] + '-' + fecha[0]
        }
    },
    {
        name: 'concepto',
        title: 'Concepto'
    },
    {
        name: 'monto',
        title: 'Cargo',
        align: 'right',
        formatter: function(row) {
            if (row.saldo || row.saldo_total) {
                return ''
            }

            if (row.c_a === '+') {
                return (+row.monto).toLocaleString('en-US')
            } else {
                return ''
            }
        }
    },
    {
        name: 'monto',
        title: 'Abono',
        align: 'right',
        formatter: function(row) {
            if (row.saldo || row.saldo == 0) {
                return 'Saldo: <strong>' + (+row.saldo).toLocaleString('en-US') + '</strong>'
            } else if (row.saldo_total || row.saldo_total == 0) {
                return 'Total: <strong>' + (+row.saldo_total).toLocaleString('en-US') + '</strong>'
            }

            if (row.c_a === '-') {
                return (+row.monto).toLocaleString('en-US')
            } else {
                return ''
            }
        }
    }
]"/>