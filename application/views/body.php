<h5 class="text-center" style="margin: -18px 0 3px 0;" id="main-table-title"></h5>

<div class="row">
    <div class="col-md-4">
        <div class="container-fluid">
            <h4 id="titulo_reporte">
                Operaciones
            </h4>
        </div>
    </div>
    <div class="col-md-8">
        <div id="datepicker" class="container-fluid">
            <?php $this->load->view('datepicker'); ?>
        </div>
    </div>
</div>

<div style="margin: 6px;"></div>

<div id="operaciones" class="container-fluid">
    <?php $this->load->view('operaciones'); ?>
</div>

<div id="cgm" class="container-fluid">
    <?php $this->load->view('cgm'); ?>
</div>

<div id="cga" class="container-fluid">
    <?php $this->load->view('cga'); ?>
</div>

<div id="cta" class="container-fluid">
    <?php $this->load->view('cta'); ?>
</div>

<?php $this->load->view('facturas'); ?>

<?php $this->load->view('contenedor'); ?>



<div id="log" class="container-fluid">
    <?php $this->load->view('users_log'); ?>
</div>
