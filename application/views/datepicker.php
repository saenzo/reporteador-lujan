<div class="form-inline pull-right">
    <div style="margin-left: 13px;">
        <label @click="buscar_por = 'fecha'">
            <strong>Fecha</strong>
            <input type="radio" name="buscarpor" checked>
        </label>
        &nbsp;
        <label @click="buscar_por = 'referencia'">
            <strong>Referencia / pedimento</strong>
            <input type="radio" name="buscarpor"> 
        </label>
    </div>
    <div class="form-group" v-show="buscar_por==='fecha'">
        <div class='input-group date datetimepicker'>
            <span class="input-group-addon">
                <span class="fa fa-calendar"></span>
            </span>
            <input data-fecha="inicial" v-model="fecha_inicial" type='text' class="form-control" placeholder="Fecha inicial" />
        </div>
    </div>
    <div class="form-group" v-show="buscar_por==='fecha'">
        <div class='input-group date datetimepicker'>
            <span class="input-group-addon">
                <span class="fa fa-calendar"></span>
            </span>
            <input data-fecha="final" v-model="fecha_final" type='text' class="form-control" placeholder="Fecha final" />
        </div>
    </div>


    <div class="form-group" v-show="buscar_por==='referencia'">
        <div class='input-group'>
            <span class="input-group-addon">
                <span class="fa fa-file"></span>
            </span>
            <input v-model="referencia" type='text' class="form-control" placeholder="Referencia" />
        </div>
    </div>

    <!--button type="button" class="btn btn-primary" @click="buscarPor">
        Buscar por fecha
    </button-->
    <button type="button" class="btn btn-primary" @click="buscar">
        <i class="fa fa-search"></i>
    </button>
    <button type="button" class="btn btn-success" @click="descargar">
        <i class="fa fa-download"></i>
    </button>
</div>
