<v-table
    v-show="open"
    ref="table"
    url="/cgm"
    :field="[
    {
        name: 'pedimento',
        title: 'Pedimento'
    },
    {
        name: 'trafico',
        title: 'Referencia'
    },
    {
        name: 'no_ctagas',
        title: 'Cuenta-PDF',
        xlsx: false,
        formatter(row, no_ctagas) {
            let href = '/visor/cgm?_=' + row.trafico

            return `<a href='${href}' target='_blank' title='${row.trafico}'>
                ${no_ctagas}
            </a>`
        }
    },
    {
        name: 'no_ctagas',
        title: 'Cuenta-XML',
        xlsx: false,
        formatter(row, no_ctagas) {
            let href = '/visor/xcgm?_=' + row.trafico

            return `<a href='${href}' target='_blank' title='${row.trafico}'>
                ${no_ctagas}
            </a>`
        }
    },
    {
        name: 'fecha',
        title: 'Fecha',
        formatter: function(row) {
            var fecha = row.fecha.replace('00:00:00.000','')

            fecha = fecha.split('-')

            return fecha[2] + '-' + fecha[1] + '-' + fecha[0]
        }
    },
    {
        name: 'mercancia',
        width: '500px',
        title: 'Descripción Mcías.'
    },
    {
        name: 'honorarios',
        title: 'Honorarios',
        align: 'right',
        formatter: function(row, val) {
            return (+val).toLocaleString('en-US')
        }
    },
    {
        name: 'complementarios',
        title: 'Serv. Compl.',
        align: 'right',
        formatter: function(row, val) {
            return (+val).toLocaleString('en-US')
        }
    },
    {
        name: 'base_iva',
        title: 'Base IVA',
        align: 'right',
        formatter: function(row, val) {
            return (+val).toLocaleString('en-US')
        }
    },
    {
        name: 'iva_al',
        title: '% IVA',
        align: 'right',
        formatter: function(row, iva) {
            return '% ' + iva
        }
    },
    {
        name: 'iva',
        title: 'IVA Cobrado',
        align: 'right',
        formatter: function(row, val) {
            return (+val).toLocaleString('en-US')
        }
    },
    {
        name: 'subtotal',
        title: 'Sub total',
        align: 'right',
        formatter: function(row, val) {
            return (+val).toLocaleString('en-US')
        }
    },
    {
        name: 'phpcc',
        title: 'Pagos Hechos Cta. Cte.',
        align: 'right',
        formatter: function(row, val) {
            return (+val).toLocaleString('en-US')
        }
    },
    {
        name: 'anticipo',
        title: 'Remesa para gasto (-)',
        align: 'right',
        formatter: function(row, val) {
            return (+val).toLocaleString('en-US')
        }
    },
    {
        name: '',
        title: 'Saldo',
        align: 'right',
        formatter: function(row, val) {
            var saldo = +row.subtotal + +row.phpcc - +row.anticipo

            return (+saldo).toLocaleString('en-US')
        }
    }
]"/>