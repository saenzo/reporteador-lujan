<!-- Modal -->
<div id="modal-facturas" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
                <h4 class="modal-title">
                    Facturas - {{num_refe}}
                </h4>
            </div>
            <div class="modal-body">
                <v-table
                    ref="table"
                    url="/facturas"
                    :field="[{
                        name: 'CONS_FACT',
                        title: 'Consecutivo',
                        width: '100px'
                    },{
                        name: 'NUM_FACT',
                        title: 'Factura'
                    },{
                        name: 'FEC_FACT',
                        title: 'Fecha',
                        width: '150px',
                        formatter(row) {
                            return Operaciones.obtenerFecha(row.FEC_FACT)
                        }
                    },{
                        name: 'CVE_PRO',
                        title: 'Clave proveedor'
                    },{
                        name: 'NOM_PRO',
                        title: 'Nombre proveedor'
                    },{
                        name: 'VAL_EXTR',
                        title: 'Valor extr'
                    },{
                        name: 'MON_FACT',
                        title: 'Monto factura'
                    }    
                    ]"
                />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cerrar
                </button>
            </div>
        </div>

    </div>
</div>