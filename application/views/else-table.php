<div style="margin: 50px;"></div>
<div v-if="cargando">
    <h4 class="text-center">
        Cargando por favor espere
        <i class="fa fa-spin fa-spinner fa-lg"></i>
    </h4>
</div>
<div v-if="!cargando && table.tableRows.length < 1">
    <h4 class="text-center">
        No hay registros para mostrar
        <i class="fa fa-search"></i>
    </h4>
</div>