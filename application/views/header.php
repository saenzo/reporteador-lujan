<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <i class="icon-bar"></i>
                <i class="icon-bar"></i>
                <i class="icon-bar"></i>
            </button>
            <a class="navbar-brand" style="margin-top: -12px;">
                <img  style="opacity: 0.9; width: 200px;"
                    src="/public/img/logo.png" class="img-responsive">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php $this->load->view('reporte') ?>
            </ul>
            <?php if (user('other') === true): ?>
            <ul class="nav navbar-nav">
                <li>
                    <a  style="color: white;" href="/admin" target="_blank" rel="noopener noreferrer">
                        ADMIN *
                    </a>
                </li>
            </ul>
            <?php endif; ?>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle tooltipster-left" title="Opciones de usuario" data-toggle="dropdown">
                        <span class="fa fa-user"></span>
                        <?php echo user('user'); ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo base_url(); ?>logout">
                                <span class="fa fa-sign-out"></span>
                                Cerrar sesión
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
