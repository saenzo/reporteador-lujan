<?php echo doctype(); ?>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv='expires' content='-1' />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    	<meta name="viewport" content="width=device-width, initial-scale=1" />

		<title> Sergio Lujan y CIA </title>

		<link rel="stylesheet" href="/node_modules/w3-css/4/w3pro.css">
		<link rel="stylesheet" href="/public/css/bootswatch/paper.min.css">
		<link rel="stylesheet" href="/node_modules/font-awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/public/css/login.css">
	</head>
	<body style="display: none;">
		<div class="container-fluid" id="login-main">
      		<div class="panel panel-primary w3-card-2 w3-round" id="panel">
  				<div class="panel-heading">
		        	<img 
						style="opacity: 0.9; margin-top: 4px; width: 250px"
						src="/public/img/logo.png" class="img-responsive center-block">
  				</div>
      			<div class="panel-body">
					<form class="form-horizontal">
					  	<div class="form-group">
					    	<div class="container-fluid">
					      		<div class="input-group" style="margin-bottom: 3px;">
					        		<span class="input-group-addon">
					        			<span class="fa fa-institution"></span>
					        		</span>
					        		<input type="text" class="form-control input-lg" name="client" placeholder="empresa" required autofocus />
					      		</div>
					      		<div class="input-group" style="margin-bottom: 3px;">
					        		<span class="input-group-addon">
					        			<span class="fa fa-user"></span>&nbsp;
					        		</span>
					        		<input type="email" class="form-control input-lg" name="user" placeholder="usuario" required />
					      		</div>
					      		<div class="input-group">
					        		<span class="input-group-addon">
					        			<span class="fa fa-key"></span>
					        		</span>
					        		<input type="password" class="form-control input-lg" name="password" placeholder="contrasena" required />
					      		</div>
					    	</div>
					  	</div>
					  	<h5 class="text-muted text-center" style="opacity: 0;" id="logError">
					  		verifique sus datos
					  	</h5>
					  	<button type="submit" class="btn btn-primary btn-block">
					  		Iniciar sesion
					  	</button>
					</form>
      			</div>
    		</div>
		</div>
		<canvas class="background"></canvas>
  		<script src="/node_modules/particlesjs/dist/particles.min.js"></script>
	</body>
	<footer>
		<script>
			var base_url = "<?php echo base_url(); ?>";
		</script>

		<script src="/public/js/ie10-viewport-bug-workaround.js"></script>
		<script src="/node_modules/jquery/dist/jquery.min.js"></script>
		<script
			src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
			integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
			crossorigin="anonymous"></script>
		<script src="/public/js/login.js"></script>

		<script>
			jQuery(() => {
				setTimeout(() => {
					jQuery('body').show()
				}, 100);
			})
		</script>
	</footer>
</html>
