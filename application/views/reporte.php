<li class="dropdown" id="reporte">
    <a href 
        style="color: white;"
        @click.prevent class="dropdown-toggle tooltipster-right" title="Seleccionar un reporte" data-toggle="dropdown">
        <i class="fa fa-puzzle-piece"></i>
        Reportes
        <i class="caret"></i>
    </a>
    <ul class="dropdown-menu">
        <li>
            <a href="#" @click.prevent="operaciones"  class="tooltipster-right" title="Reporte de operaciones">
                Operaciones
            </a>
        </li>
        <li>
            <a @click.prevent="cgm" class="tooltipster-right" title="Reporte de cuenta de gastos mexicana">
                Cuenta gastos MX
            </a>
        </li>
        <li>
            <a @click.prevent="cga" class="tooltipster-right" title="Reporte de cuenta de gastos americana">
                Cuenta gastos USA
            </a>
        </li>
        <li>
            <a @click.prevent="cta" class="tooltipster-right" title="Ver estado de cuenta">
                Estado de cuenta
            </a>
        </li>
        <li>
            <a href="http://trafico.slujanc.com" target="_blank" title="Ir a bodega">
                Bodega
            </a>
        </li>
        <?php
            $_x = $this->session->userdata(LOGIN);
            if (in_array("admin", $_x["client"])):
        ?>
        <li>
            <a href @click.prevent="log" class="tooltipster-right" title="Inicios de sesión">
                Log de usuarios
            </a>
        </li>
        <?php endif; ?>
    </ul>
</li>