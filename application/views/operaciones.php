<v-table
    v-show="open"
    ref="table"
    url="/operaciones"
    :field="[
    {
        name: 'NUM_REFE',
        title: 'Expediente',
        width: '100px',
        align: 'center',
        xlsx: false,
        formatter(row) {
            let href = '/expediente?_=' + row.NUM_REFE

            return `<a href='${href}' target='_blank' title='${row.NUM_REFE}'>
                <i class='fa fa-file'></i>
            </a>`
        }
    },
    {
        name: 'NUM_REFE',
        title: 'Zip',
        width: '100px',
        align: 'center',
        xlsx: false,
        formatter(row) {
            let href = '/visor/download?ref=' + row.NUM_REFE

            return `<a href='${href}' target='_blank' title='${row.NUM_REFE}'>
                <i class='fa fa-file-archive-o'></i>
            </a>`
        }
    },
    {
        name: 'NUM_PEDI',
        title: 'Pedimento'
    },
    {
        name: 'NUM_REFE',
        title: 'Referencia',
        width: '110px'
    },
    {
        name: 'ADU_ENTR',
        title: 'Aduana de entrada'
    },
    {
        name: 'REG_ADUA',
        title: 'Regimen'
    },
    {
        name: 'CVE_PEDI',
        title: 'Clave ped'
    },
    {
        name: 'PAT_AGEN',
        title: 'Agente'
    },
    {
        name: 'FEC_ENTR',
        title: 'Fecha de entrada',
        formatter(row) {
            return Operaciones.obtenerFecha(row.FEC_ENTR)
        }
    },
    {
        name: 'FEC_PAGO',
        title: 'Fecha de pago',
        formatter(row) {
            return Operaciones.obtenerFecha(row.FEC_PAGO)
        }
    },
    {
        name: 'MAR_NUME',
        title: 'Marcas (no comerciales)'
    },
    {
        name: 'PES_BRUT',
        title: 'Peso bruto'
    },
    {
        name: 'CVE_IMPO',
        title: 'Clave cliente'
    }, 
    {
        name: 'NOM_IMP',
        title: 'Cliente'
    },
    {
        name: 'TIP_MOVA',
        title: 'Tipo de moneda'
    },
    {
        name: 'TIP_CAMB',
        title: 'Tipo de cambio',
        formatter(row, val) {
            return currency.format(val)
        }
    },
    {
        name: 'TOT_INCR',
        title: 'Total incrementables',
        formatter(row, val) {
            return currency.format(val)
        }
    },
    {
        name: 'TOT_ADVP',
        title: 'Advalorem partida',
        formatter(row, val) {
            return currency.format(val)
        }
    },
    {
        name: 'TOT_ADV',
        title: 'Total advalorem',
        formatter(row, val) {
            return currency.format(val)
        }
    },
    {
        name: 'TOT_DTA',
        title: 'Total DTA',
        formatter(row, val) {
            return currency.format(val)
        }
    },
    {
        name: 'TOT_IVAP',
        title: 'IVA partida',
        formatter(row, val) {
            return currency.format(val)
        }
    },
    {
        name: 'TOT_IVA',
        title: 'Total IVA',
        formatter(row, val) {
            return currency.format(val)
        }
    },
    {
        name: 'CONTPED_OTRO',
        title: 'Otros',
        formatter(row, val) {
            return currency.format(val)
        }
    },
    {
        name: 'TOT_EFEC',
        title: 'Total efectivo',
        formatter(row, val) {
            return currency.format(val)
        }
    }, 
    {
        name: 'TOT_OTRO',
        title: 'Total otro',
        formatter(row, val) {
            return currency.format(val)
        }
    }, 
    {
        name: 'TOT_PAGO',
        title: 'Pago total',
        formatter(row, val) {
            return currency.format(val)
        }
    },
    {
        name: 'FIR_ELEC',
        title: 'Firma electronica validación'
    },
    {
        name: 'CAN_BULT',
        title: 'Cantidad de bultos'
    },
    {
        name: 'NUM_PART',
        title: 'Numero de partida'
    }, 
    {
        name: 'VAL_DLLS',
        title: 'Valor aduana dls',
        formatter(row, val) {
            return currency.format(val)
        }
    }, 
    {
        name: 'VAL_COME',
        title: 'Valor comercial',
        formatter(row, val) {
            return currency.format(val)
        }
    },
    {
        name: 'VAL_NORM',
        title: 'Valor aduana',
        formatter(row, val) {
            return currency.format(val)
        }
    },
    {
        name: 'FAC_AJUS',
        title: 'Factor ajuste'
    }, 
    {
        name: 'DES_MERC',
        title: 'Mercancia'
    }, 
    {
        name: 'CAN_FACT',
        title: 'Cantidad comercial'
    },
    {
        name: 'MON_FACT',
        title: 'Valor com partida dls',
        formatter(row, val) {
            return currency.format(val)
        }
    },
    {
        name: 'VAL_COMF',
        title: 'Valor comercial partida',
        formatter(row, val) {
            return currency.format(val)
        }
    },
    {
        name: 'VAL_NORF',
        title: 'Valor aduana partida',
        formatter(row, val) {
            return currency.format(val)
        }
    }, 
    {
        name: 'VAL_AGRE',
        title: 'Valor agregado partida'
    },
    {
        name: 'MON_DTAP',
        width: '100px',
        title: 'DTA partida',
        formatter(row, val) {
            return currency.format(val)
        }
    }, 
    {
        name: 'FRACCION',
        title: 'Fracción arancelaria'
    }, 
    {
        name: 'TAS_ADV',
        title: 'Tasa ADV'
    }, 
    {
        name: 'TAS_IVA',
        title: 'Tasa IVA'
    }, 
    {
        name: 'NUM_FACT',
        title: 'Facturas',
        width: '300px',
        formatter(row) {
            if (row.NUM_FACT) {
                return `<a href='#ver-facturas' data-refe='${row.NUM_REFE}' data-toggle='modal' data-target='#modal-facturas'>
                    ${row.NUM_FACT.join(' | ')}
                </a>`
            } else {
                return ''
            }
        }
    },
    {
        name: 'FEC_FACT',
        title: 'Fecha facturas',
        width: '300px',
        formatter(row) {
           return row.FEC_FACT.join(' | ')
        }
    },
    {
        name: 'NOM_PRO',
        title: 'Proveedor',
        width: '300px',
        formatter(row) {
           return row.NOM_PRO.join(' | ')
        }
    },
    {
        name: 'CAN_TARI',
        title: 'Cantidad tarifa'
    },
    {
        name: 'PAI_ORIG',
        title: 'País de origen'
    }, {
        name: 'PAI_VEND',
        title: 'País vendedor'
    },
    {
        name: 'UNI_TARI',
        title: 'Unidad tarifa'
    }, {
        name: 'UNI_FACT',
        title: 'Unidad comercial'
    },
    {
        name: 'CVE_VALO',
        title: 'Valoración'
    },
    {
        name: 'CVE_VINC',
        title: 'Vinculación'
    }, 
    {
        name: 'CAS_TLCS',
        title: 'TLC'
    },
    {
        name: 'COM_TLC',
        title: 'País TLC'
    },
    {
        name: 'TOT_VEHI',
        title: 'Total vechiculos'
    }, 
    {
        name: 'NUM_CONT',
        title: 'Contenedor',
        width: '300px',
        formatter(row) {
            if (row.NUM_CONT) {
                return `<a href='#ver-contenedor' data-refe='${row.NUM_REFE}' data-toggle='modal' data-target='#modal-contenedor'>
                    ${row.NUM_CONT.join(' | ')}
                </a>`
            } else {
                return ''
            }
        }
    },
    {
        name: 'IDE_TRANSP',
        title: 'Placas'
    }, 
    {
        name: 'NUM_SELLOS',
        title: 'Sellos / Candados',
        width: '300px',
        formatter(row) {
           return row.NUM_SELLOS.join(' | ')
        }
    }
]"/>