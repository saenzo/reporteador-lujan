<v-table
    v-show="open"
    ref="table"
    url="/cga"
    :field="[
    {
        name: 'pedimento',
        title: 'Pedimento'
    },
    {
        name: 'trafico',
        title: 'Referencia'
    },
    {
        name: 'no_ctagas',
        title: 'Expense Bill No.'
    },
    {
        name: 'fecha',
        title: 'Date',
        formatter: function(row) {
            let date = row.fecha.replace('00:00:00.000','')
                date = date.split('-')

            return `${date[1]}-${date[2]}-${date[0]}`
        }
    },
    {
        name: 'mercancia',
        title: 'Description'
    },
    {
        name: 'carrier',
        title: 'Shipper'
    },
    {
        name: 'factura',
        title: 'Invoice No.'
    },
    {
        name: 'fecha_fac',
        title: 'Invoice Date',
        formatter: function(row) {
            let date = row.fecha_fac.replace('00:00:00.000','')
                date = date.split('-')

            return `${date[1]}-${date[2]}-${date[0]}`
        }
    },
    {
        name: 'val_fac',
        title: 'Invoice Value',
        align: 'right',
        formatter: function(row, val) {
            return (+val).toLocaleString('en-US')
        }
    },
    {
        name: 'trailer',
        title: 'Trailer No.'
    },
    {
        name: 'fecha_cru',
        title: 'Fecha cruce',
        formatter: function(row) {
            let date = row.fecha_cru.replace('00:00:00.000','')
                date = date.split('-')

            return `${date[1]}-${date[2]}-${date[0]}`
        }
    },
    {
        name: 'ttotal',
        title: 'Total Charges',
        align: 'right',
        formatter: function(row, val) {
            return (+val).toLocaleString('en-US')
        }
    }
]"/>

<!--
    {
        name: 'no_ctagas',
        title: 'Cuenta-PDF',
        xlsx: false,
        formatter(row, no_ctagas) {
            let href = '/visor/cga?_=' + row.trafico

            return `<a href='${href}' target='_blank' title='${row.trafico}'>
                ${no_ctagas}
            </a>`
        }
    },
-->