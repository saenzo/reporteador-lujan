<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pedimento_dom extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    // pedimento
    public function getTable($e)
    {   
        // almacenar las columnas de la tabla
        $columns = array(
            array('field' => 'INFO'    , 'title' => 'vista'),
            array('field' => 'FRAC'    , 'title' => 'fracciones'),
            array('field' => 'FAC'     , 'title' => 'facturas'),
            array('field' => 'NUM_REFE', 'title' => 'referencia', 'sortable' => true),
            array('field' => 'CVE_IMPO', 'title' => 'clave cliente', 'sortable' => true),
            array('field' => 'NOM_IMP' , 'title' => nbs(16).'nombre del cliente'.nbs(16), 'sortable' => true),
            array('field' => 'IMP_EXPO', 'title' => 'tipo operacion', 'sortable' => true),
            array('field' => 'TIP_PEDI', 'title' => 'tipo pedimento', 'sortable' => true),
            array('field' => 'ADU_DESP', 'title' => 'aduana de despacho', 'sortable' => true),
            array('field' => 'PAT_AGEN', 'title' => 'patente', 'sortable' => true),
            array('field' => 'NUM_PEDI', 'title' => 'numero de pedimento', 'sortable' => true),
            array('field' => 'ADU_ENTR', 'title' => 'aduana de entrada', 'sortable' => true),
            array('field' => 'FEC_ENTR', 'title' => 'fecha de entrada', 'sortable' => true),
            array('field' => 'TIP_CAMB', 'title' => 'tipo de cambio', 'sortable' => true, 'align' => 'right'),
            array('field' => 'CVE_PEDI', 'title' => 'clave pedimento', 'sortable' => true),
            array('field' => 'REG_ADUA', 'title' => 'regimen', 'sortable' => true),
            array('field' => 'DES_ORIG', 'title' => 'destino de la mercancia', 'sortable' => true),
            array('field' => 'MAR_NUME', 'title' => 'marcas (no comerciales)', 'sortable' => true),
            array('field' => 'PES_BRUT', 'title' => 'peso bruto', 'sortable' => true),
            array('field' => 'MTR_ENTR', 'title' => 'medio de transporte entrada', 'sortable' => true),
            array('field' => 'MTR_ARRI', 'title' => 'medio de transporte arribo', 'sortable' => true),
            array('field' => 'MTR_SALI', 'title' => 'medio de transporte salida', 'sortable' => true),
            array('field' => 'TIP_MOVA', 'title' => 'tipo de moneda', 'sortable' => true),
            array('field' => 'VAL_DLLS', 'title' => 'valor en dolares', 'sortable' => true, 'align' => 'right'),
            array('field' => 'VAL_COME', 'title' => 'valor comercial', 'sortable' => true, 'align' => 'right'),
            array('field' => 'TOT_INCR', 'title' => 'incrementables', 'sortable' => true, 'align' => 'right'),
            array('field' => 'TOT_DEDU', 'title' => 'deducibles', 'sortable' => true, 'align' => 'right'),
            array('field' => 'VAL_NORM', 'title' => 'valor normal', 'sortable' => true, 'align' => 'right'),
            array('field' => 'FAC_AJUS', 'title' => 'factor ajuste', 'sortable' => true, 'align' => 'right'),
            array('field' => 'FEC_PAGO', 'title' => 'fecha de pago', 'sortable' => true),
            array('field' => 'PAR_2DTA', 'title' => 'derecho tramite aduanero', 'sortable' => true),
            array('field' => 'FAC_ACTU', 'title' => 'factor actualizacion', 'sortable' => true),
            array('field' => 'NUM_FRAC', 'title' => 'fraciones', 'sortable' => true),
            array('field' => 'TOT_EFEC', 'title' => 'total efectivo', 'sortable' => true, 'align' => 'right'),
            array('field' => 'TOT_OTRO', 'title' => 'total otro', 'sortable' => true, 'align' => 'right'),
            array('field' => 'FIR_ELEC', 'title' => 'firma electronica validacion', 'sortable' => true),
            array('field' => 'NUM_CAND', 'title' => 'candados', 'sortable' => true),
            array('field' => 'TOT_VEHI', 'title' => 'total vahiculos', 'sortable' => true),
            array('field' => 'CVE_BANC', 'title' => 'clave de banco', 'sortable' => true),
            array('field' => 'NUM_CAJA', 'title' => 'numero de caja banco', 'sortable' => true),
            array('field' => 'DIA_PAGO', 'title' => 'fecha de pago', 'sortable' => true),
            array('field' => 'HOR_PAGO', 'title' => 'hora de pago', 'sortable' => true),
            array('field' => 'TOT_PAGO', 'title' => 'pago total', 'sortable' => true, 'align' => 'right'),
            array('field' => 'CAN_BULT', 'title' => 'cantidad de bultos', 'sortable' => true),
            array('field' => 'FIR_PAGO', 'title' => 'firma de pago', 'sortable' => true),
            array('field' => 'NUM_OPER', 'title' => 'numero de operacion', 'sortable' => true)
        );

        // renglones de la tabla
        $rows = array();
        
        $user_mk = user('mk');

        $imp_name = array();

        // recorrer los renglones de la tabla
        foreach ($e as $values) {

            // obtener el nombre completo del importador
            if ( array_key_exists($values['CVE_IMPO'], $imp_name) ) {

                $values['NOM_IMP'] = $imp_name[$values['CVE_IMPO']];

            } else {

                // consultar el nombre del importador
                $values['NOM_IMP'] = imp_name($values['CVE_IMPO'], 'fb');

                // se almacena la clave del importador para no volver a consultar la base de datos
                array_push($imp_name, $values['NOM_IMP']);
            }
            
            // preparar un renglon nuevo
            $row = array(
                // informacion sobre un archivo
                'INFO' => "
                    <button title='ver y desglosar contenido' 
                            class='btn btn-link btn-xs tooltipster' 
                            value='{$values['NUM_REFE']}' 
                            name='row-info' imp='{$values['CVE_IMPO']}'>
                        <i class='fa fa-eye'></i>
                    </button>
                ",
                // obtener fracciones sobre una referencia           
                'FRAC' => "
                    <button title='ver tabla de fracciones - {$values['NUM_FRAC']} -'
                            class='btn btn-link btn-xs tooltipster'
                            ref='{$values['NUM_REFE']}'
                            name='frac' imp='{$values['CVE_IMPO']}'
                            data-toggle='modal' data-target='.bs-frac'>
                        <i class='fa fa-circle'></i>
                    </button>
                ",
                // obtener fracciones sobre una referencia            
                'FAC' => "
                    <button title='ver tabla de facturas'
                            class='btn btn-link btn-xs tooltipster'
                            ref='{$values['NUM_REFE']}'
                            name='fac'
                            imp='{$values['CVE_IMPO']}'
                            data-toggle='modal'
                            data-target='.bs-fac'>
                    <i class='fa fa-file'></i>
                </button>",
                // ver todos los documentos en classifile
                'NUM_REFE' => '
                    <a href="http://api.classifile.mx/es/viewer?m='.$user_mk.'&c=doc&k=ref:'.$values['NUM_REFE'].'"
                       target="_blank"
                       class="tooltipster"
                       title="ver documentos para '.$values['NUM_REFE'].'">
                        '.$values['NUM_REFE'].'
                    </a>
                ',
                'CVE_IMPO' => $values['CVE_IMPO'],
                'NOM_IMP'  => $values['NOM_IMP'],
                'IMP_EXPO' => $values['IMP_EXPO'],
                'TIP_PEDI' => $values['TIP_PEDI'],
                'ADU_DESP' => $values['ADU_DESP'],
                'PAT_AGEN' => $values['PAT_AGEN'],
                // ver el numero de pedimento
                'NUM_PEDI' => '
                    <a href="http://api.classifile.mx/es/viewer?m='.$user_mk.'&c=doc&k=ref:'.$values['NUM_REFE'].',tipo:PED"
                       target="_blank"
                       class="tooltipster"
                       title="ver pedimento '.$values['NUM_PEDI'].'">
                        '.$values['NUM_PEDI'].'
                    </a>
                ',
                'ADU_ENTR' => $values['ADU_ENTR'],
                'FEC_ENTR' => date('d/m/Y', strtotime($values['FEC_ENTR'])),
                'TIP_CAMB' => '$'.number_format($values['TIP_CAMB'], 2),
                'CVE_PEDI' => $values['CVE_PEDI'],
                'REG_ADUA' => $values['REG_ADUA'],
                'DES_ORIG' => $values['DES_ORIG'],
                'MAR_NUME' => $values['MAR_NUME'],
                'PES_BRUT' => $values['PES_BRUT'],
                'MTR_ENTR' => $values['MTR_ENTR'],
                'MTR_ARRI' => $values['MTR_ARRI'],
                'MTR_SALI' => $values['MTR_SALI'],
                'TIP_MOVA' => $values['TIP_MOVA'],
                'VAL_DLLS' => '$'.number_format($values['VAL_DLLS'], 2),
                'VAL_COME' => '$'.number_format($values['VAL_COME'], 2),
                'TOT_INCR' => '$'.number_format($values['TOT_INCR'], 2),
                'TOT_DEDU' => '$'.number_format($values['TOT_DEDU'], 2),
                'VAL_NORM' => '$'.number_format($values['VAL_NORM'], 2),
                'FAC_AJUS' => '$'.number_format($values['FAC_AJUS'], 2),
                'FEC_PAGO' => date('d/m/Y', strtotime($values['FEC_PAGO'])),
                'PAR_2DTA' => $values['PAR_2DTA'],
                'FAC_ACTU' => '$'.number_format($values['FAC_AJUS'], 2),
                'NUM_FRAC' => $values['NUM_FRAC'],
                'TOT_EFEC' => '$'.number_format($values['TOT_EFEC'], 2),
                'TOT_OTRO' => '$'.number_format($values['TOT_OTRO'], 2),
                'FIR_ELEC' => $values['FIR_ELEC'],
                'NUM_CAND' => $values['NUM_CAND'],
                'TOT_VEHI' => $values['TOT_VEHI'],
                'CVE_BANC' => $values['CVE_BANC'],
                'NUM_CAJA' => $values['NUM_CAJA'],
                'DIA_PAGO' => date('d/m/Y', strtotime($values['DIA_PAGO'])),
                'HOR_PAGO' => date('h:i', strtotime($values['HOR_PAGO'])),
                'TOT_PAGO' => '$'.number_format($values['TOT_PAGO'], 2),
                'CAN_BULT' => $values['CAN_BULT'],
                'FIR_PAGO' => $values['FIR_PAGO'],
                'NUM_OPER' => $values['NUM_OPER']
            );           
            
            // almacenar renglon
            $rows[] = $row;
        }

        return array('columns' => $columns, 'rows' => $rows);
    }
}
    

    