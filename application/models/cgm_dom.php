<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cgm_dom extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        
    }

    public function getTable($result)
    {   
        // definir columnas de la tabla
        $columns = array(
            array('field' => 'no_ctagas',    'sortable' => true, 'title' => 'numero cuenta gastos'),
            array('field' => 'fecha',        'sortable' => true, 'title' => 'fecha cuenta gastos'),
            array('field' => 'trafico',      'sortable' => true, 'title' => 'referencia'),
            array('field' => 'pedimento',    'sortable' => true, 'title' => 'pedimento'),
            array('field' => 'cve_pdto',     'sortable' => true, 'title' => 'clave pedimento'),
            array('field' => 'fecha_ped',    'sortable' => true, 'title' => 'fecha pedimento'),
            array('field' => 'factura',      'sortable' => true, 'title' => 'factura'),
            array('field' => 'no_cte',       'sortable' => true, 'title' => 'clave cliente'),
            array('field' => 'imp_name',     'sortable' => true, 'title' => nbs(16).'nombre del cliente'.nbs(16)),
            array('field' => 'imp_exp',      'sortable' => true, 'title' => 'tipo'),
            array('field' => 'mercancia1',   'sortable' => true, 'title' => 'descripcion'),
            array('field' => 'mercancia2',   'sortable' => true, 'title' => '&nbsp;&nbsp;&nbsp;bultos&nbsp;&nbsp;&nbsp;'),
            array('field' => 'remite2',      'sortable' => true, 'title' => 'proveedor'),
            array('field' => 'peso_kg',      'sortable' => true, 'title' => 'kilogramos'),
            array('field' => 'peso_lb',      'sortable' => true, 'title' => 'libras'),
            array('field' => 'base_grava',   'sortable' => true, 'title' => 'base gravable'),
            array('field' => 'tarifat',      'sortable' => true, 'title' => 'honorarios', 'align' => 'right'),
            array('field' => 'ivat',         'sortable' => true, 'title' => 'iva', 'align' => 'right'),
            array('field' => 'base_iva',     'sortable' => true, 'title' => 'subtotal', 'align' => 'right'),
            array('field' => 'iva_al',       'sortable' => true, 'title' => '% iva'),
            array('field' => 'ttotal',       'sortable' => true, 'title' => 'total', 'align' => 'right'),
            array('field' => 'tot110',       'sortable' => true, 'title' => 'gastos comprobados', 'align' => 'right'),
            array('field' => 'totcom',       'sortable' => true, 'title' => 'servicios complementarios', 'align' => 'right'),
            array('field' => 'dlls',         'sortable' => true, 'title' => 'cuenta de gastos americana'),
            array('field' => 'cta_ame',      'sortable' => true, 'title' => 'numero de cuenta americana')
        );
    
        // definir renglones de la tabla
        $rows = array();

        $user_mk = user('mk');

		// Se utiliza para obtener el nombre del importador
        $imp_name = array();//aactetar();

        while ( $row = odbc_fetch_array($result) ) {

            $_imp_name = array_key_exists($row['no_cte'], $imp_name) ? $imp_name[$row['no_cte']] : '-';
            
            array_push( $rows, array(
                'no_ctagas'  => '
                    <a href="http://api.classifile.mx/es/viewer?m='.$user_mk.'&c=doc&k=ref:'.substr($row['trafico'], 0, 10).',tipo:CGM" target="_blank">
                        '.$row['no_ctagas'].'
                    </a>
                ',
                'fecha'      => date('d/m/Y', strtotime($row['fecha'])),
                'trafico'    => '
                    <a href="http://api.classifile.mx/es/viewer?m='.$user_mk.'&c=doc&k=ref:'.substr($row['trafico'], 0, 10).'" target="_blank">
                        '.substr($row['trafico'], 0, 10).'
                    </a>
                ',
                'pedimento'  => $row['pedimento'],
                'cve_pdto'   => $row['cve_pdto'],
                'fecha_ped'  => date('d/m/Y', strtotime($row['fecha_ped'])),
                'factura'    => substr($row['factura'], 0, 11),
                'no_cte'     => $row['no_cte'],
                'imp_name'   => $_imp_name,
                'imp_exp'    => $row['imp_exp'],
                'mercancia1' => substr($row['mercancia1'], 0, 11),
                'mercancia2' => substr($row['mercancia2'], 0, 11),
                'remite2'    => substr($row['remite2'], 0, 11),
                'peso_kg'    => $row['peso_kg'],
                'peso_lb'    => $row['peso_lb'],
                'base_grava' => $row['base_grava'],
                'tarifat'    => '$'.number_format($row['tarifat'], 2),
                'ivat'       => '$'.number_format($row['ivat'], 2),
                'base_iva'   => '$'.number_format($row['base_iva'], 2),
                'iva_al'     => '% '.$row['iva_al'],
                'ttotal'     => '$'.number_format($row['ttotal'], 2),
                'tot110'     => '<button class="btn btn-link btn-xs" no_ctagas="'.$row['no_ctagas'].'" no_serie="'.$row['no_serie'].'" name="cgm-aacgmere">
                                    $'.number_format($row['tot110'], 2).'
                                 </button>',
                'totcom'     => '<button class="btn btn-link btn-xs" no_ctagas="'.$row['no_ctagas'].'" no_serie="'.$row['no_serie'].'" name="cgm-aacgcomp">
                                    $'.number_format($row['totcom'], 2).'
                                 </button>',
                'dlls'       => '$'.number_format($row['dlls'], 2),
                'cta_ame'  => '
                    <a href="http://api.classifile.mx/es/viewer?m='.$user_mk.'&c=doc&k=ref:'.substr($row['trafico'], 0, 10).',tipo:CGA" target="_blank">
                        '.$row['cta_ame'].'
                    </a>
                '
            ));

        }

        return array('columns' => $columns, 'rows' => $rows);
    }

    public function aacgmere($result)
    {   

        $columns = array(
            array('field' => 'no_ctagas', 'sortable' => true, 'title' => 'numero cuenta gastos'),
            array('field' => 'descrip',   'sortable' => true, 'title' => 'descripcion'),
            array('field' => 'monto',     'sortable' => true, 'title' => 'monto', 'align' => 'right'),
            array('field' => 'fecha',     'sortable' => true, 'title' => 'fecha', 'align' => 'right'),
        
        );

        $rows = array();

        while ( $row = odbc_fetch_array($result) ) {

            array_push( $rows, array(
                'no_ctagas' => $row['no_ctagas'],
                'descrip'   => $this->tocatcta($row['no_pos']),
                'monto'     => '$'.number_format($row['monto'], 2),
                'fecha'     => date('d/m/Y', strtotime($row['fecha']))
            ));

        }


        return array('columns' => $columns, 'rows' => $rows);

        // tabla de conceptops == tocatcta
    }

    public function aacgcomp($result)
    {   

        $columns = array(
            array('field' => 'no_ctagas', 'sortable' => true, 'title' => 'numero cuenta gastos'),
            array('field' => 'descrip',   'sortable' => true, 'title' => 'descripcion'),
            array('field' => 'monto',     'sortable' => true, 'title' => 'monto', 'align' => 'right'),
            array('field' => 'fecha',     'sortable' => true, 'title' => 'fecha', 'align' => 'right'),
        
        );

        $rows = array();

        while ( $row = odbc_fetch_array($result) ) {

            array_push( $rows, array(
                'no_ctagas' => $row['no_ctagas'],
                'descrip'   => $this->aacatcom($row['no_pos']),
                'monto'     => '$'.number_format($row['monto'], 2),
                'fecha'     => date('d/m/Y', strtotime($row['fecha']))
            ));

        }


        return array('columns' => $columns, 'rows' => $rows);

        // tabla de conceptos == aacatcomp
    }

    public function tocatcta($no) 
    {   
        $db = dbase_open('e:\\visualvial\\contab\\tocatcta.dbf', 0);

        $obs = '';

        if ($db) {

            $número_registros = dbase_numrecords($db);

            for ($i = 1; $i <= $número_registros; $i++) {

                $fila = dbase_get_record_with_names($db, $i);
                
                if ( $fila['SUB_CTA'] == $no && $fila['CUENTA'] == '110' ) {
                    $obs = $fila['NOMBRE'];
                }
            }

            dbase_close($db);
        }

        return $obs;

    }

    public function aacatcom($no) 
    {   
        $db = dbase_open('e:\\visualvial\\contab\\aacatcom.dbf', 0);

        $obs = '';

        if ($db) {

            $número_registros = dbase_numrecords($db);

            for ($i = 1; $i <= $número_registros; $i++) {

                $fila = dbase_get_record_with_names($db, $i);
              
                if ( $fila['NO_POS'] == $no ) {
                    $obs = $fila['NOMBRE'];
                }
            }

            dbase_close($db);
        }

        return $obs;
    }

}
    

    