<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cga_dom extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getTable($result)
    {
        // columnas de la tabla
        $columns = array(
            array('field' => 'no_ctagas',    'sortable' => true, 'title' => 'numero cuenta gastos' ),
            array('field' => 'fecha',        'sortable' => true, 'title' => 'fecha cuenta gastos' ),
            array('field' => 'trafico',      'sortable' => true, 'title' => 'referencia' ),
            array('field' => 'factura',      'sortable' => true, 'title' => 'factura' ),
            array('field' => 'no_cte',       'sortable' => true, 'title' => 'clave cliente' ),
            array('field' => 'imp_name',     'sortable' => true, 'title' => nbs(16).'nombre del cliente'.nbs(16)),
            array('field' => 'mercancia1',   'sortable' => true, 'title' => 'descripcion' ),
            array('field' => 'ttotal',       'sortable' => true, 'title' => 'total', 'align' => 'right'),
        );

        // renglones de la tabla

        $rows = array();

        $user_mk = user('mk');

        $imp_name = aactetar();

        while ( $row = odbc_fetch_array($result) ) {

            $_imp_name = array_key_exists($row['no_cte'], $imp_name) ? $imp_name[$row['no_cte']] : '-';

            array_push( $rows, array(
                'no_ctagas'  => '
                    <a href="http://api.classifile.mx/es/viewer?m='.$user_mk.'&c=doc&k=ref:'.substr($row['trafico'], 0, 10).',tipo:CGA" target="_blank">
                        '.$row['no_ctagas'].'
                    </a>
                ',
                'fecha'      => date('d/m/Y', strtotime($row['fecha'])),
                'trafico'    => '
                    <a href="http://api.classifile.mx/es/viewer?m='.$user_mk.'&c=doc&k=ref:'.substr($row['trafico'], 0, 10).'" target="_blank">
                        '.substr($row['trafico'], 0, 10).'
                    </a>
                ',
                'factura'    => substr($row['factura'], 0, 11),
                'no_cte'     => $row['no_cte'],
                'imp_name'   => $_imp_name,
                'mercancia1' => substr($row['mercancia1'], 0, 11),
                'ttotal'     => '$'.number_format($row['ttotal'], 2)
            ));

        }

        return array('columns' => $columns, 'rows' => $rows);
    }

}
    

    