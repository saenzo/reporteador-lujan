<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fracciones_dom extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    // pedimento
    public function getTable($e)
    {   
        // almacenar las columnas de la tabla
        $columns = array(
            array('field' => 'NUM_REFE', 'title' => 'referencia', 'sortable' => true),
            array('field' => 'NUM_PART', 'title' => 'numero de parte', 'sortable' => true),
            array('field' => 'FRACCION', 'title' => 'fraccion', 'sortable' => true),
            array('field' => 'MON_FACT', 'title' => 'monto factura', 'sortable' => true),
            array('field' => 'TIP_MONE', 'title' => 'tipo moneda', 'sortable' => true),
            array('field' => 'UNI_TARI', 'title' => 'unidad de tarifa', 'sortable' => true),
            array('field' => 'UNI_FACT', 'title' => 'unidad comercial', 'sortable' => true),
            array('field' => 'PAI_ORIG', 'title' => 'pais origen', 'sortable' => true),
            array('field' => 'PAI_VEND', 'title' => 'pais vendedor', 'sortable' => true),
            array('field' => 'CVE_VALO', 'title' => 'valoracion', 'sortable' => true),
            array('field' => 'CVE_VINC', 'title' => 'vinculacion', 'sortable' => true),
            array('field' => 'CAS_TLCS', 'title' => 'TLC', 'sortable' => true),
            array('field' => 'ADVAL',    'title' => 'advalorem', 'sortable' => true),
            array('field' => 'MON_DTAP', 'title' => 'DTA', 'sortable' => true),
            array('field' => 'PORC_IVA', 'title' => 'Tasa IVA', 'sortable' => true),
            array('field' => 'MON_IEPS', 'title' => 'IEPS', 'sortable' => true),
            array('field' => 'MON_COM1', 'title' => 'cuota compensatoria', 'sortable' => true),
            array('field' => 'VAL_AGRE', 'title' => 'valor agregado', 'sortable' => true),
            array('field' => 'MAR_MERC', 'title' => 'marcas meracncia', 'sortable' => true),
            array('field' => 'MOD_MERC', 'title' => 'modelo mercancia', 'sortable' => true),
            array('field' => 'COD_PROD', 'title' => 'codigo de producto', 'sortable' => true),
            array('field' => 'VAL_NORF', 'title' => 'valor normal', 'sortable' => true),
            array('field' => 'VAL_COMF', 'title' => 'valor comercial', 'sortable' => true),
            array('field' => 'CAN_FACT', 'title' => 'cantidad comercial', 'sortable' => true),
            array('field' => 'CAN_TARI', 'title' => 'cantidad tarifa', 'sortable' => true),
            array('field' => 'COM_TLC', 'title'  => 'pais TLC', 'sortable' => true)
        );

        // renglones de la tabla
        $rows = array();
        
        $user_mk = user('mk');

        // recorrer los renglones de la tabla
        foreach ($e as $values) {

            // preparar un renglon nuevo
            $row = array(
                // ver todos los documentos en classifile
                'NUM_REFE' => '
                    <a href="http://api.classifile.mx/es/viewer?m='.$user_mk.'&c=doc&k=ref:'.$values['NUM_REFE'].'"
                       target="_blank"
                       class="tooltipster"
                       title="ver documentos">
                        '.$values['NUM_REFE'].'
                    </a>
                ',
                'NUM_PART' => $values['NUM_PART'],
                'FRACCION' => $values['FRACCION'],
                'MON_FACT' => '$'.number_format($values['MON_FACT'], 2),
                'TIP_MONE' => $values['TIP_MONE'],
                'UNI_TARI' => $values['UNI_TARI'],
                'UNI_FACT' => $values['UNI_FACT'],
                'PAI_ORIG' => $values['PAI_ORIG'],
                'PAI_VEND' => $values['PAI_VEND'],
                'CVE_VALO' => $values['CVE_VALO'],
                'CVE_VINC' => $values['CVE_VINC'],
                'CAS_TLCS' => $values['CAS_TLCS'],
                'ADVAL'    => $values['ADVAL'],
                'MON_DTAP' => '$'.number_format($values['MON_DTAP'], 2),
                'PORC_IVA' => $values['PORC_IVA'].'%',
                'MON_IEPS' => '$'.number_format($values['MON_IEPS'], 2),
                'MON_COM1' => '$'.number_format($values['MON_COM1'], 2),
                'VAL_AGRE' => '$'.number_format($values['VAL_AGRE'], 2),
                'MAR_MERC' => $values['MAR_MERC'],
                'MOD_MERC' => $values['MOD_MERC'],
                'COD_PROD' => $values['COD_PROD'],
                'VAL_NORF' => '$'.number_format($values['VAL_NORF'], 2),
                'VAL_COMF' => '$'.number_format($values['VAL_COMF'], 2),
                'CAN_FACT' => '$'.number_format($values['CAN_FACT'], 2),
                'CAN_TARI' => '$'.number_format($values['CAN_TARI'], 2),
                'COM_TLC'  => $values['COM_TLC']
            );           
            
            // almacenar renglon
            $rows[] = $row;
        }

        return array('columns' => $columns, 'rows' => $rows);
    }
}
    

    