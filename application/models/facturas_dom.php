<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Facturas_dom extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    // pedimento
    public function getTable($e)
    {   
        // almacenar las columnas de la tabla
        $columns = array(
            array('field' => 'NUM_REFE', 'title' => 'referencia', 'sortable' => true),
            array('field' => 'CONS_FACT', 'title' => 'consecutivo', 'sortable' => true),
            array('field' => 'NUM_FACT', 'title' => 'numero de factura', 'sortable' => true),
            array('field' => 'FEC_FACT', 'title' => 'fecha de la factura', 'sortable' => true),
            array('field' => 'ICO_FACT', 'title' => 'incoterm', 'sortable' => true),
            array('field' => 'VAL_DLLS', 'title' => 'valor dolares', 'sortable' => true),
            array('field' => 'VAL_EXTR', 'title' => 'valor moneda extranjera', 'sortable' => true),
            array('field' => 'CVE_PROV', 'title' => 'clave proveedor', 'sortable' => true),
            array('field' => 'MON_FACT', 'title' => 'moneda', 'sortable' => true),
            array('field' => 'NUM_PEDI', 'title' => 'pedido', 'sortable' => true),
            array('field' => 'PES_BRUT', 'title' => 'peso bruto', 'sortable' => true),
            array('field' => 'CAN_BULT', 'title' => 'bultos', 'sortable' => true)
        );

        // renglones de la tabla
        $rows = array();
        
        $user_mk = user('mk');

        // recorrer los renglones de la tabla
        foreach ($e as $values) {

            // preparar un renglon nuevo
            $row = array(
                // ver todos los documentos en classifile
                'NUM_REFE' => '
                    <a href="http://api.classifile.mx/es/viewer?m='.$user_mk.'&c=doc&k=ref:'.$values['NUM_REFE'].'"
                       target="_blank"
                       class="tooltipster"
                       title="ver documentos">
                        '.$values['NUM_REFE'].'
                    </a>
                ',
                'CONS_FACT' => $values['CONS_FACT'],
                'NUM_FACT' => '
                    <a href="http://api.classifile.mx/es/viewer?m='.$user_mk.'&c=doc&k=ref:'.$values['NUM_REFE'].',tipo:FAC" 
                       target="_blank"
                       class="tooltipster"
                       title="ver factura numero '.$values['NUM_FACT'].'">
                        '.$values['NUM_FACT'].'
                    </a>
                ',
                'FEC_FACT' => date('d/m/Y', strtotime($values['FEC_FACT'])),
                'ICO_FACT' => $values['ICO_FACT'],
                'VAL_DLLS' => '$'.number_format($values['VAL_DLLS'], 2),
                'VAL_EXTR' => '$'.number_format($values['VAL_EXTR'], 2),
                'CVE_PROV' => $values['CVE_PROV'],
                'MON_FACT' => $values['MON_FACT'],
                'NUM_PEDI' => $values['NUM_PEDI'],
                'PES_BRUT' => $values['PES_BRUT'],
                'CAN_BULT' => $values['CAN_BULT']
            );           
            
            // almacenar renglon
            $rows[] = $row;
        }

        return array('columns' => $columns, 'rows' => $rows);
    }
}
    

    