<?php

function ci() {

    $ci =& get_instance();

    return $ci;
}
// obtener un dato especifico del usuario
function user($param)
{	
	$ci =& get_instance();

	$ses = $ci->session->userdata(LOGIN);

	return $ses[$param];
}

// para manejar las casillas de excel
function num2alpha($num)
{
    $numeric = ($num - 1) % 26;

    $letter = chr(65 + $numeric);

    $num2 = intval(($num - 1) / 26);

    if ($num2 > 0) {
        return num2alpha($num2) . $letter;
    } else {
        return $letter;
    }
}

// obtener nombre completo del importador utilizando su clave
function imp_name($key, $db)
{   
    // key: clave del importador
    // db: tipo de base de datos

    switch ($db) {

        case 'fb': // firebird

            $strq = "SELECT NOM_IMP FROM CTRAC_CLIENT WHERE CVE_IMP = '{$key}'";

            $result = ci()->load->database('fbird', true)->query($strq)->result(true);

            return count($result) >= 1 ? $result[0]['NOM_IMP'] : '-';

            break;
        
        default:
            # code...
            break;
    }
}

// Obtener el nombre del importador
function aactetar() 
{   
    $db = false;

    try
    {
		$db = dbase_open('c:\\contab_tmp\\aactetar.dbf', 0);
        //$db = dbase_open('e:\\visualvial\\contab\\aactetar.dbf', 0);
    }
    catch(exception $ex)
    {}

    //$db = dbase_open("E:\\visualvial\\contab\\aacte.dbf", 0);

    $imp_name = array();

    if ($db) {

        $numrecords = dbase_numrecords($db);

        for ($i = 1; $i <= $numrecords; $i++) {

            $field = dbase_get_record_with_names($db, $i);
          
            
            $imp_name[$field['NO_CTE']] = $field['NOMBRE'];
            
        }

        dbase_close($db);
    }

    return $imp_name;
}