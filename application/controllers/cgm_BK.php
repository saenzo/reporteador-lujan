<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cgm extends CI_Controller
{ 
	public function __construct()
    {
        parent::__construct();

        // establecer una conexion
		
        $this->odbc = odbc_connect ('dbf', '', '', SQL_CUR_USE_ODBC) or exit(0);
    }
	
	
	public function tmp()
	{
		

	
		$db = false;

		try
		{
			// ejecutar sentencia SQL para obtener un resultado
            $result = odbc_exec($this->odbc, 'SELECT * from aacgmex') or exit(0);
			//$db = dbase_open('\\\Server\\e\\visualvial\\contab\\aacgmex.DBF', 0);
		}
		catch(exception $ex)
		{
			echo "error";
		}

		$imp_name = array();

		if ($db) {

			$numrecords = dbase_numrecords($db);

			for ($i = 1; $i <= $numrecords; $i++) {

				$field = dbase_get_record_with_names($db, $i);
				
				print_r($field);
				
			}

			dbase_close($db);
		}
	}
	
    // obtener tabla cuenta gastos MX por rango de fechas
    public function bydate()
    {       
        // rango de fechas
        $date_one = $this->input->post('date_one');
        $date_two = $this->input->post('date_two');

        // salir si no estan definidos
        if ( ! $date_one || ! $date_two) exit(0);

        $found = false;

        $table = array();

        $this->load->model('cgm_dom');

        $date_one = date( 'Y-m-d', strtotime($date_one) );
        $date_two = date( 'Y-m-d', strtotime($date_two . '+1 days') );

        while ( ! $found && strtotime($date_one) < strtotime($date_two) ) {

            $where = "WHERE BETWEEN(FECHA, {d '$date_one'}, {d '$date_two'})";

            // ejecutar sentencia SQL para obtener un resultado
            $result = odbc_exec($this->odbc, $this->strsql($where)) or exit(0);

            $table = $this->cgm_dom->getTable($result);
                
            // restar un dia a la fecha original | rango de fecha
            $date_two = date( 'Y-m-d', strtotime($date_two . '-1 days') );

            // terminar si existen registros
            if ( count($table['rows']) >= 1) $found = true;        
        }

        // cerrar conexion
        odbc_close($this->odbc);

        exit(json_encode($table));
    }

    // obtener tabla cuenta gastos MX por referencia
    public function byref()
    {   
        // recibe un trafico o cuenta de gastos
        $ref = $this->input->post('ref');

        // salir si no esta definida
        if ( ! $ref ) exit(0);

        // verificar si es por cuenta de gastos
        if ( is_numeric($ref) && strlen($ref) >= 1 ) {

            $where = "WHERE NO_CTAGAS = {$ref}";
        
        // verificar si es por trafico
        } elseif ( strlen($ref) >= 1) {

            $where = "WHERE TRAFICO = '{$ref}'";
        
        } else {

            exit(0);
        } 

        // ejecutar sentencia SQL para obtener un resultado
        $result = odbc_exec($this->odbc, $this->strsql($where)) or exit(0);

        $this->load->model('cgm_dom');

        $table = $this->cgm_dom->getTable($result);

        odbc_close($this->odbc);

        exit( json_encode($table) );
    }

    // obtener tabla cuenta gastos MX por rango de fechas
    public function toExcelByDate()
    {       
        // rango de fechas
        $date_one = $this->input->post('date_one');
        $date_two = $this->input->post('date_two');

        // salir si no estan definidos
        if ( ! $date_one || ! $date_two) exit(0);

        $found = false;

        $rows = array();

        $date_one = date( 'Y-m-d', strtotime($date_one) );
        $date_two = date( 'Y-m-d', strtotime($date_two . '+1 days') );

        while ( ! $found && strtotime($date_one) < strtotime($date_two) ) {

            $where = "WHERE BETWEEN(FECHA, {d '$date_one'}, {d '$date_two'})";

            // ejecutar sentencia SQL para obtener un resultado
            $result = odbc_exec($this->odbc, $this->strsql($where)) or exit(0);

            while ( $row = odbc_fetch_array($result)) $rows[] = $row;
                
            // restar un dia a la fecha original | rango de fecha
            $date_two = date( 'Y-m-d', strtotime($date_two . '-1 days') );

            // terminar si existen registros
            if ( count($rows >= 1) ) $found = true;        
        }

        // cerrar conexion
        odbc_close($this->odbc);

        $this->excel($rows);

    }

    // obtener tabla cuenta gastos MX por referencia
    public function toExcelByRef()
    {   
        // recibe un trafico o cuenta de gastos
        $ref = $this->input->post('ref');

        // salir si no esta definida
        if ( ! $ref ) exit(0);

        // verificar si es por cuenta de gastos
        if ( is_numeric($ref) && strlen($ref) >= 1 ) {

            $where = "WHERE NO_CTAGAS = {$ref}";
        
        // verificar si es por trafico
        } elseif ( strlen($ref) >= 1) {

            $where = "WHERE TRAFICO = '{$ref}'";
        
        } else {

            exit(0);
        } 

        // ejecutar sentencia SQL para obtener un resultado
        $result = odbc_exec($this->odbc, $this->strsql($where)) or exit(0);

        $rows = array();
        
        while ( $row = odbc_fetch_array($result)) $rows[] = $row;

        // cerrar conexion
        odbc_close($this->odbc);

        $this->excel($rows);
    }

    // generar una sentencia SQL
    public function strsql($where)
    {

        // verificar el tipo de usuario
        if ( ! in_array('admin', user('client')) ) {

            // obtener clientes a los que puede accesar el usuario
            $keys = implode(',', user('other'));
            
            $_where = " AND NO_CTE IN ({$keys})";
        
        } else {
            $_where = "";
        }

        // sentencia SQL
        return 'SELECT 
            NO_CTAGAS,
            FECHA,
            TRAFICO,
            PEDIMENTO,
            CVE_PDTO,
            FECHA_PED,
            FACTURA,
            NO_CTE,
            IMP_EXP,
            MERCANCIA1,
            MERCANCIA2,
            REMITE2,
            PESO_KG,
            PESO_LB,
            BASE_GRAVA,
            TARIFAT,
            IVAT,
            BASE_IVA,
            IVA_AL,
            TTOTAL,
            DLLS,
            CTA_AME,
            NO_SERIE,
            TOTCOM,
            TOT110 FROM aacgmexs ' . $where . $_where . ' ORDER BY FECHA DESC
        ';
    }

    public function aacgmere()
    {
        // recibe un trafico o cuenta de gastos
        $no_ctagas = $this->input->post('no_ctagas');

        // numero de serie
        $no_serie = $this->input->post('no_serie');

        // sentencia SQL
        $strsql = "SELECT TOP 512
            NO_CTAGAS,
            MONTO,
            FECHA,
            NO_POS
            FROM aacgmere WHERE NO_CTAGAS = {$no_ctagas} AND NO_SERIE = {$no_serie} ORDER BY FECHA DESC
        ";
        
        // ejecutar sentencia SQL para obtener un resultado
        $result = odbc_exec($this->odbc, $strsql) or exit(0);
        
        $this->load->model('cgm_dom');

        $response = $this->cgm_dom->aacgmere($result);

        odbc_close($this->odbc);

        $table = json_encode($response);

        exit($table);
    }

    public function aacgcomp()
    {
        // recibe un trafico o cuenta de gastos
        $no_ctagas = $this->input->post('no_ctagas');

        // numero de serie
        $no_serie = $this->input->post('no_serie');

        // sentencia SQL
        $strsql = "SELECT TOP 512
            NO_CTAGAS,
            DESCRIP,
            MONTO,
            FECHA,
            NO_POS
            FROM aacgcomp WHERE NO_CTAGAS = {$no_ctagas} AND NO_SERIE = {$no_serie} ORDER BY FECHA DESC
        ";
        
        // ejecutar sentencia SQL para obtener un resultado
        $result = odbc_exec($this->odbc, $strsql) or exit(0);
        
        $this->load->model('cgm_dom');

        $response = $this->cgm_dom->aacgcomp($result);

        odbc_close($this->odbc);

        $table = json_encode($response);

        exit($table);
    }

    public function excel($e)
    {   
        // cargar creador de archivos excel
        $this->load->library('excel');

        // establecer propiedades
        $this->excel->getProperties()->setCreator("Sergio Lujan");
        $this->excel->getProperties()->setLastModifiedBy("Sergio Lujan");
        $this->excel->getProperties()->setTitle("Office 2007 XLSX Document");
        $this->excel->getProperties()->setSubject("Office 2007 XLSX Document");
        $this->excel->getProperties()->setDescription("document for Office 2007 XLSX, generated by Sergio Lujan");

        // generar datos
        $this->excel->setActiveSheetIndex(0);


        $this->excel->getActiveSheet()->SetCellValue('b2', 'numero cuenta gastos');
        $this->excel->getActiveSheet()->SetCellValue('c2', 'fecha cuenta gastos');
        $this->excel->getActiveSheet()->SetCellValue('d2', 'trafico');
        $this->excel->getActiveSheet()->SetCellValue('e2', 'pedimento');
        $this->excel->getActiveSheet()->SetCellValue('f2', 'clave pedimento');
        $this->excel->getActiveSheet()->SetCellValue('g2', 'fecha pedimento');
        $this->excel->getActiveSheet()->SetCellValue('h2', 'factura');
        $this->excel->getActiveSheet()->SetCellValue('i2', 'clave cliente');
        $this->excel->getActiveSheet()->SetCellValue('j2', 'tipo');
        $this->excel->getActiveSheet()->SetCellValue('k2', 'descripcion');
        $this->excel->getActiveSheet()->SetCellValue('l2', 'bultos');
        $this->excel->getActiveSheet()->SetCellValue('m2', 'proveedor');
        $this->excel->getActiveSheet()->SetCellValue('n2', 'kilogramos');
        $this->excel->getActiveSheet()->SetCellValue('o2', 'libras');
        $this->excel->getActiveSheet()->SetCellValue('p2', 'base gravable');
        $this->excel->getActiveSheet()->SetCellValue('q2', 'honorarios');
        $this->excel->getActiveSheet()->SetCellValue('r2', 'iva');
        $this->excel->getActiveSheet()->SetCellValue('s2', 'subtotal');
        $this->excel->getActiveSheet()->SetCellValue('t2', '% iva');
        $this->excel->getActiveSheet()->SetCellValue('u2', 'total');
        $this->excel->getActiveSheet()->SetCellValue('v2', 'gastos comprobados');
        $this->excel->getActiveSheet()->SetCellValue('w2', 'cuenta de gastos americana');
        $this->excel->getActiveSheet()->SetCellValue('x2', 'numero de cuenta americana');
              
         
        $r = 3; // renglon

        foreach ($e as $row) {

            $this->excel->getActiveSheet()->SetCellValue('b' .$r, $row['no_ctagas']);
            $this->excel->getActiveSheet()->SetCellValue('c' .$r, $row['fecha']);
            $this->excel->getActiveSheet()->SetCellValue('d' .$r, $row['trafico']);
            $this->excel->getActiveSheet()->SetCellValue('e' .$r, $row['pedimento']);
            $this->excel->getActiveSheet()->SetCellValue('f' .$r, $row['cve_pdto']);
            $this->excel->getActiveSheet()->SetCellValue('g' .$r, $row['fecha_ped']);
            $this->excel->getActiveSheet()->SetCellValue('h' .$r, substr($row['factura'], 0, 11));
            $this->excel->getActiveSheet()->SetCellValue('i' .$r, $row['no_cte']);
            $this->excel->getActiveSheet()->SetCellValue('j' .$r, $row['imp_exp']);
            $this->excel->getActiveSheet()->SetCellValue('k' .$r, substr($row['mercancia1'], 0, 11));
            $this->excel->getActiveSheet()->SetCellValue('l' .$r, substr($row['mercancia2'], 0, 11));
            $this->excel->getActiveSheet()->SetCellValue('m' .$r, substr($row['remite2'], 0, 11));
            $this->excel->getActiveSheet()->SetCellValue('n' .$r, $row['peso_kg']);
            $this->excel->getActiveSheet()->SetCellValue('o' .$r, $row['peso_lb']);
            $this->excel->getActiveSheet()->SetCellValue('p' .$r, $row['base_grava']);
            $this->excel->getActiveSheet()->SetCellValue('q' .$r, $row['tarifat']);
            $this->excel->getActiveSheet()->SetCellValue('r' .$r, $row['ivat']);
            $this->excel->getActiveSheet()->SetCellValue('s' .$r, $row['base_iva']);
            $this->excel->getActiveSheet()->SetCellValue('t' .$r, $row['iva_al']);
            $this->excel->getActiveSheet()->SetCellValue('u' .$r, $row['ttotal']);
            $this->excel->getActiveSheet()->SetCellValue('v' .$r, $row['totcom']);
            $this->excel->getActiveSheet()->SetCellValue('w' .$r, $row['dlls']);
            $this->excel->getActiveSheet()->SetCellValue('x' .$r, $row['cta_ame']);

            $r++;
        }       

        // titulo de la hoja
        $this->excel->getActiveSheet()->setTitle('gastos');
                
        // Cuardar como archivo Excel 2007
        $objWriter = new PHPExcel_Writer_Excel2007($this->excel);

        $objWriter->save(str_replace('.php', '.xlsx', __FILE__));

        // cargar helper de archivos
        $this->load->helper('file');

        ob_clean();

        header('Content-Type: ' . get_mime_by_extension('.xls'));
        header('Content-Disposition: attachment; filename="cg_mexicana.xls"');
        header('Cache-Control: max-age=0');

        ob_flush();

        $objWriter->save('php://output');
    }

}
