<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cga extends CI_Controller
{ 
	public function __construct()
    {
        parent::__construct();

        // Establecer las fechas por defecto
        $this->setDates();
    }
	
    public function index()
    {
        $db = $this->load->database('cga', true);

        // Busqueda por defecto
        $where = "fecha BETWEEN '{$this->date_one}' AND '{$this->date_two}'";

        // Verificar si hay una referncia definida
        if (isset($_GET['num_refe'])) {
            $pedimento = trim($_GET['num_refe']);

            $where = "pedimento = '{$pedimento}'";
        }

        if (!in_array('admin', user('client'))) {
        	$user_keys = array();

            foreach (user('other') as $value) {
                $value = str_replace("'", '', $value);
                
                if (is_numeric($value)) { 
                    array_push($user_keys, $value);
                }
            }

        	// obtener clientes a los que puede accesar el usuario
            $keys = implode(',', $user_keys);
            
            $where = "no_cte IN ({$keys}) AND {$where}";
        }

        $query = "SELECT * FROM facturasusa WHERE {$where}";

        $data = array( 'rows' => array(), 'total' => 0 );

        $data['rows'] = $db->query($query)->result(true);
        $data['total'] = count($data['rows']);
        
        exit(json_encode($data));
    }

    private function setDates()
    {
        // almacenar fecha de inicio
        $date_one = $this->input->get('date_one');

        // almacenar fecha 2, rango de fecha
        $date_two = $this->input->get('date_two');

        // ferificar que la fecha uno se encuentre definida
        // Almacenar fecha dos, rango de fecha
        if (!$date_one) {
            $date_one = date('Y-m-d', time());
        } else {          
            $date_one = date('Y-m-d',strtotime($date_one));            
        }

        // Almacenar fecha dos, rango de fecha
        if (!$date_two) {
            $date_two = date('Y-m-d',strtotime($date_one. "+1 days"));
        } else {          
            $date_two = date('Y-m-d',strtotime($date_two));            
        }

        $this->date_one = $date_one;
        $this->date_two = $date_two;
    }
}
