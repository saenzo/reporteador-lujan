<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cga extends CI_Controller
{
 
	public function __construct()
    {
        parent::__construct();

         // establecer una conexion
        $this->odbc = odbc_connect ('dbf', '', '', SQL_CUR_USE_ODBC) or exit(0);
    }

    // obtener tabla cuenta gastos americana
    public function bydate()
    {       
        // rango de fechas
        $date_one = $this->input->post('date_one');
        $date_two = $this->input->post('date_two');

        // salir si no estan definidos
        if ( ! $date_one || ! $date_two) exit(0);

        $found = false;

        $table = array();

        $this->load->model('cga_dom');

        $date_one = date( 'Y-m-d', strtotime($date_one) );
        $date_two = date( 'Y-m-d', strtotime($date_two . '+1 days') );

        while ( ! $found && strtotime($date_one) < strtotime($date_two) ) {

            $where = "WHERE BETWEEN(FECHA, {d '$date_one'}, {d '$date_two'})";

            // ejecutar sentencia SQL para obtener un resultado
            $result = odbc_exec($this->odbc, $this->strsql($where)) or exit(0);

            $table = $this->cga_dom->getTable($result);
                
            // restar un dia a la fecha original | rango de fecha
            $date_two = date( 'Y-m-d', strtotime($date_two . '-1 days') );

            // terminar si existen registros
            if ( count($table['rows']) >= 1) $found = true;        
        }

        // cerrar conexion
        odbc_close($this->odbc);

        exit(json_encode($table));
    }

    // obtener tabla cuenta gastos USA por referencia
    public function byref()
    {   
        // recibe un trafico o cuenta de gastos
        $ref = $this->input->post('ref');

        // salir si no esta definida
        if ( ! $ref ) exit(0);

        // verificar si es por cuenta de gastos
        if ( is_numeric($ref) && strlen($ref) >= 1 ) {

            $where = "WHERE NO_CTAGAS = {$ref}";
        
        // verificar si es por trafico
        } elseif ( strlen($ref) >= 1) {

            $where = "WHERE TRAFICO = '{$ref}'";
        
        } else {

            exit(0);
        } 

        // ejecutar sentencia SQL para obtener un resultado
        $result = odbc_exec($this->odbc, $this->strsql($where)) or exit(0);

        $this->load->model('cga_dom');

        $table = $this->cga_dom->getTable($result);

        odbc_close($this->odbc);

        exit( json_encode($table) );
    }

    // obtener tabla cuenta gastos MX por rango de fechas
    public function toExcelByDate()
    {       
        // rango de fechas
        $date_one = $this->input->post('date_one');
        $date_two = $this->input->post('date_two');

        // salir si no estan definidos
        if ( ! $date_one || ! $date_two) exit(0);

        $found = false;

        $rows = array();

        $date_one = date( 'Y-m-d', strtotime($date_one) );
        $date_two = date( 'Y-m-d', strtotime($date_two . '+1 days') );

        while ( ! $found && strtotime($date_one) < strtotime($date_two) ) {

            $where = "WHERE BETWEEN(FECHA, {d '$date_one'}, {d '$date_two'})";

            // ejecutar sentencia SQL para obtener un resultado
            $result = odbc_exec($this->odbc, $this->strsql($where)) or exit(0);

            while ( $row = odbc_fetch_array($result)) $rows[] = $row;
                
            // restar un dia a la fecha original | rango de fecha
            $date_two = date( 'Y-m-d', strtotime($date_two . '-1 days') );

            // terminar si existen registros
            if ( count($rows >= 1) ) $found = true;        
        }

        // cerrar conexion
        odbc_close($this->odbc);

        $this->excel($rows);

    }

    // obtener tabla cuenta gastos MX por referencia
    public function toExcelByRef()
    {   
        // recibe un trafico o cuenta de gastos
        $ref = $this->input->post('ref');

        // salir si no esta definida
        if ( ! $ref ) exit(0);

        // verificar si es por cuenta de gastos
        if ( is_numeric($ref) && strlen($ref) >= 1 ) {

            $where = "WHERE NO_CTAGAS = {$ref}";
        
        // verificar si es por trafico
        } elseif ( strlen($ref) >= 1) {

            $where = "WHERE TRAFICO = '{$ref}'";
        
        } else {

            exit(0);
        } 

        // ejecutar sentencia SQL para obtener un resultado
        $result = odbc_exec($this->odbc, $this->strsql($where)) or exit(0);

        $rows = array();
        
        while ( $row = odbc_fetch_array($result)) $rows[] = $row;

        // cerrar conexion
        odbc_close($this->odbc);

        $this->excel($rows);
    }

    // generar una sentencia SQL
    public function strsql($where)
    {

        // verificar el tipo de usuario
        if ( ! in_array('admin', user('client')) ) {

            // obtener clientes a los que puede accesar el usuario
            $keys = implode(',', user('other'));
            
            $_where = " AND NO_CTE IN ({$keys})";
        
        } else {
            $_where = "";
        }

        // sentencia SQL
        return 'SELECT 
            NO_CTAGAS,
            FECHA,
            TRAFICO,
            FACTURA,
            NO_CTE,
            MERCANCIA1,
            TTOTAL
            FROM aacgmex ' . $where . $_where . ' ORDER BY FECHA DESC
        ';
    }

    public function excel($e)
    {   
        // cargar creador de archivos excel
        $this->load->library('excel');

        // establecer propiedades
        $this->excel->getProperties()->setCreator("Sergio Lujan");
        $this->excel->getProperties()->setLastModifiedBy("Sergio Lujan");
        $this->excel->getProperties()->setTitle("Office 2007 XLSX Document");
        $this->excel->getProperties()->setSubject("Office 2007 XLSX Document");
        $this->excel->getProperties()->setDescription("document for Office 2007 XLSX, generated by Sergio Lujan");

        // generar datos
        $this->excel->setActiveSheetIndex(0);


        $this->excel->getActiveSheet()->SetCellValue('b2', 'numero cuenta gastos');
        $this->excel->getActiveSheet()->SetCellValue('c2', 'fecha cuenta gastos');
        $this->excel->getActiveSheet()->SetCellValue('d2', 'trafico');
        $this->excel->getActiveSheet()->SetCellValue('e2', 'factura');
        $this->excel->getActiveSheet()->SetCellValue('f2', 'clave cliente');
        $this->excel->getActiveSheet()->SetCellValue('g2', 'descripcion');
        $this->excel->getActiveSheet()->SetCellValue('h2', 'total');
              
         
        $r = 3; // renglon

        foreach ($e as $row) {

            $this->excel->getActiveSheet()->SetCellValue('b' .$r, $row['no_ctagas']);
            $this->excel->getActiveSheet()->SetCellValue('c' .$r, $row['fecha']);
            $this->excel->getActiveSheet()->SetCellValue('d' .$r, $row['trafico']);
            $this->excel->getActiveSheet()->SetCellValue('e' .$r, substr($row['factura'], 0, 11));
            $this->excel->getActiveSheet()->SetCellValue('f' .$r, $row['no_cte']);
            $this->excel->getActiveSheet()->SetCellValue('g' .$r, substr($row['mercancia1'], 0, 11));
            $this->excel->getActiveSheet()->SetCellValue('h' .$r, $row['ttotal']);

            $r++;
        }       

        // titulo de la hoja
        $this->excel->getActiveSheet()->setTitle('gastos');
                
        // Cuardar como archivo Excel 2007
        $objWriter = new PHPExcel_Writer_Excel2007($this->excel);

        $objWriter->save(str_replace('.php', '.xlsx', __FILE__));

        // cargar helper de archivos
        $this->load->helper('file');

        ob_clean();

        header('Content-Type: ' . get_mime_by_extension('.xls'));
        header('Content-Disposition: attachment; filename="cg_usa.xls"');
        header('Cache-Control: max-age=0');

        ob_flush();

        $objWriter->save('php://output');
    }

}
