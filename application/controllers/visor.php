<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Visor extends CI_Controller
{ 
	public function __construct()
    {
        parent::__construct();

        $this->load->helper('download');
        $this->load->library('zip');
    }

    public function download() {
        if (!$this->session->userdata(LOGIN)) {
            $this->load->view('login');
            return;
        }
        
        $query = array();
        $query['find'] = array();
        $query['limit'] = 1;
        $query['find']['keywords.ref'] = '/'.$_GET['ref'].'/i';
        $query['find']['keywords.tipo'] = 'EXP - expediente completo';

        if (is_array(user('other')) && count(user('other')) > 0) {
            $query['find']['keywords.cte'] = array();
            $query['find']['keywords.cte']['$in'] = user('other');
        }

        $query = json_encode($query);
        $query = str_replace("'","",$query);
        $query = str_replace(" ","%20",$query);

        $url = 'https://api.classifile.mx/es/ws/file/download?usr=lujan&pwd=16sljf7!*&data='.$query;
        
        $content = file_get_contents($url);
        
        $this->zip->add_data($_GET['ref'].'.pdf', $content);

        // Descargar zip
        $this->zip->download(date("dmyHis").'.zip');

        exit("<script> window.close(); </script>");
    }

    public function cgm()
    {
        if (!isset($_GET['_'])) {
            exit('No direct script access allowed');
        }

        $mk = user('mk');
        $refe = $_GET['_'];

        $url = "https://api.classifile.mx/es/viewer?m={$mk}&c=doc&k=ref:{$refe},tipo:CGM - cuenta de gastos mexicana";
        
        $this->load->view('expediente', array('url' => $url));
    }

    public function xcgm()
    {
        if (!isset($_GET['_'])) {
            exit('No direct script access allowed');
        }

        $mk = user('mk');
        $refe = $_GET['_'];

        $url = "https://api.classifile.mx/es/viewer?m={$mk}&c=doc&k=ref:{$refe},tipo:XCGM - xml cuenta de gastos mexicana";
        
        $this->load->view('expediente', array('url' => $url));
    }

    public function cga()
    {
        if (!isset($_GET['_'])) {
            exit('No direct script access allowed');
        }

        $mk = user('mk');
        $refe = $_GET['_'];

        $url = "https://api.classifile.mx/es/viewer?m={$mk}&c=doc&k=ref:{$refe},tipo:CGA - cuenta de gastos americana";
        
        $this->load->view('expediente', array('url' => $url));
    }

    public function xcga()
    {
        if (!isset($_GET['_'])) {
            exit('No direct script access allowed');
        }

        $mk = user('mk');
        $refe = $_GET['_'];

        $url = "https://api.classifile.mx/es/viewer?m={$mk}&c=doc&k=ref:{$refe},tipo:XCGA - xml cuenta de gastos americana";
        
        $this->load->view('expediente', array('url' => $url));
    }
}
