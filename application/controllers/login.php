<?php if ( ! defined('BASEPATH')) exit("No direct script access allowed");

class Login extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
       
        // verificacion para accesar ala funcion index
        if ( $this->session->userdata(LOGIN) || (
           ! $this->input->post('client')    ||
           ! $this->input->post('user')      ||
           ! $this->input->post('password')     )
        )	header('Location:' . base_url());
		
    }

	public function index()
	{
		// obtener lista de los usuarios
		$usrs = json_decode(file_get_contents('__$users__.json'), true);

		// almacenar los datos del usuario
		$client = $this->input->post('client'); // nombre del cliente empresa u organizacion
		$user = $this->input->post('user'); // correo
		$password = $this->input->post('password'); // contrasena

		// para saber si se encontro al usuario
		$match = false;

		// barrer base de datos para buscar a usuario
		foreach ($usrs as $i => $e) {

			// verificar con todos los parametros
			if (in_array($client, $e['client']) && $e['user'] === $user && $e['password'] === $password) {

				// colocar en verdadero
		   		$match = true;

		   		if ( is_array($usrs[$i]['other']) ) {

		   			foreach ( $usrs[$i]['other'] as $l => $value ) {

						$usrs[$i]['other'][$l] = "'{$value}'";

					}

		   		}

		   		// crear una variable de sesion
		   		$this->session->set_userdata(LOGIN, $usrs[$i]);

		   		// terminar ciclo
		    	break;
			}

		}

		$this->users_log($user, $client);

		// devolver resultados de busqueda
		exit(json_encode($match));
	}

	public function users_log($user, $client)
	{
		try {
			$fileSrc = "C:\\server\\static\\users_log.json";

			if ($_SERVER['SERVER_NAME'] === 'reporteador.local') {
				$fileSrc = "C:\\Users\\Hector\\Documents\\Apache\\www\\server\\static\\users_log.json";
			}
			
			if (!file_exists($fileSrc))
			{
				$logFile = fopen($fileSrc, "w");

				fwrite($logFile, json_encode(array()));

				fclose($logFile);
			}

			$userData = file_get_contents($fileSrc);
			
			$userData = json_decode($userData, true);

		    $this->load->library('user_agent');

			if ($this->agent->is_browser())
			{
			    $agent = $this->agent->browser().' '.$this->agent->version();
			}
			elseif ($this->agent->is_robot())
			{
			    $agent = $this->agent->robot();
			}
			elseif ($this->agent->is_mobile())
			{
			    $agent = $this->agent->mobile();
			}
			else
			{
			    $agent = 'Unidentified User Agent';
			}

			array_push($userData, 
				array(
					"client" => $client,
					"user" => $user,
					"date" => date("d/m/Y h:i:s"),
					"browser" => $agent,
					"ip" => $this->input->ip_address()
					));
			
			file_put_contents($fileSrc, json_encode($userData));			

        } catch (exception $ex) {}
	}
}
