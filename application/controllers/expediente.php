<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expediente extends CI_Controller
{ 
	public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if (!isset($_GET['_'])) {
            exit('No direct script access allowed');
        }

        $mk = user('mk');
        $refe = $_GET['_'];

        $url = "https://api.classifile.mx/es/viewer?m={$mk}&c=doc&k=ref:{$refe},tipo:EXP - expediente completo";
        
        $this->load->view('expediente', array('url' => $url));
    }
}
