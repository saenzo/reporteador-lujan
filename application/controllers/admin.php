<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Administrar sistema
 */

class Admin extends CI_Controller {

	public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata(LOGIN) || user('other') !== true) {
            exit('No direct script access allowed');
        }
    }

    public function index()
    {
        $this->load->view('admin');
    }

    // Obtener lista de usuarios
    public function getUsers()
    {
        $users = file_get_contents('__$users__.json');
        
        $users = json_decode($users);

        exit(json_encode(array(
            'rows' => $users,
            'total' => count($users)
        )));
    }

    // Crear un usuario
    public function createUser()
    {
        // Obtener lista de usuarios
        $users = json_decode(file_get_contents('__$users__.json'), true);

        // Usuario para agregar
        $user = json_decode($_POST['user'], true);

        // Completar los datos del usuario
        $user['admin'] = false;
        $user['client'] = explode(',', $user['client']);
        $user['other'] = explode(',', $user['other']);
        $user['_id'] = $this->random();

        // Agregar usuario a la lista de usuarios
        $users[] = $user;

        // Actualizar archivo
	    if (file_put_contents('__$users__.json', json_encode($users))) {
            exit(true);
        } else {
            exit(false);
        }
        exit(json_encode($users));
    }

    // Eliminar un usuario
    public function deleteUser()
    {
         // Obtener todos los usuarios
        $users = json_decode(file_get_contents('__$users__.json'), true);

        $user_index = null;

        // Obtener el indice del usuario
        foreach ($users as $index => $user) {
            // Buscar usuario por ID
            if ($user['_id'] === $_POST['_id']) {
                $user_index = $index;
                break;
            }
        }

        if ($user_index !== null) {
            array_splice($users, $user_index, 1);
            //unset($users[$user_index]);
        } else {
            exit(false);
        }

        // Actualizar archivo
        if (file_put_contents('__$users__.json', json_encode($users))) {
            exit(true);
        } else {
            exit(false);
        }
    }

    // Editar un usuario
    public function editUser() 
    {
        // Obtener todos los usuarios
        $users = json_decode(file_get_contents('__$users__.json'), true);

        // Datos del usuario para actualizar
        $user_data = json_decode($_POST['user'], true);

        $user_data['client'] = explode(',', $user_data['client']);
        $user_data['other'] = explode(',', $user_data['other']);

        foreach ($users as &$user) {
            // Buscar usuario por ID
            if ($user['_id'] === $user_data['_id']) {
                $user = $user_data;
                break;
            }
        }

        // Actualizar archivo
        if (file_put_contents('__$users__.json', json_encode($users))) {
            exit(true);
        } else {
            exit(false);
        }
    }

    private function random()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        
        for ($i = 0; $i < $charactersLength; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return md5($randomString);
    }

     // Editar un usuario
     public function addId() 
     {
         // Obtener todos los usuarios
         $users = json_decode(file_get_contents('__$users__.json'), true);
 
         foreach ($users as &$user) {
            $user['_id'] = $this->random();
         }
 
         // Actualizar archivo
         if (file_put_contents('__$users__.json', json_encode($users))) {
             exit(true);
         } else {
             exit(false);
         }
     }
}
