<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Operaciones extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();

        $this->referencias = array();
        $this->operaciones = array();
    }

    // Obtener tabla cuenta gastos americana
    public function index()
    {
        $db = $this->load->database('fbird', true);

        $client = user('client');

        $this->setDates();

        // Busqueda por defecto
        $where = "pedime.FEC_ENTR BETWEEN '{$this->dateOne}' AND '{$this->dateTwo}'";

        // Verificar si hay una referncia definida
        if (isset($_GET['num_refe'])) {
            $num_refe = trim($_GET['num_refe']);
            $lower = strtolower($num_refe);
            $uper = strtoupper($num_refe);
            $ufirst = ucfirst($num_refe);

            $where = "(pedime.NUM_REFE = ' {$num_refe}' OR
                pedime.NUM_REFE = ' {$lower}' OR
                pedime.NUM_REFE = ' {$uper}' OR
                pedime.NUM_REFE = ' {$ufirst}' OR
                pedime.NUM_REFE = '{$num_refe}' OR
                pedime.NUM_REFE = '{$lower}' OR
                pedime.NUM_REFE = '{$uper}' OR
                pedime.NUM_REFE = '{$ufirst}' OR
                pedime.NUM_PEDI = ' {$num_refe}' OR
                pedime.NUM_PEDI = '{$num_refe}')";
        }

        if (!in_array('admin', $client)) {
        	// obtener clientes a los que puede accesar el usuario
            $keys = implode( ',', user('other') );
            
            $where = "pedime.CVE_IMPO IN ({$keys}) AND {$where}";
        }

        $query = "SELECT pedime.*, fracci.*, info.*,
        contfra_iva.*, contfra_adval.*, contped_otro.*,
        contfra_impu6.VAL_TASA AS TAS_ADV,
        contfra_impu6.TOT_IMPU AS TOT_ADVP,
        contfra_impu3.VAL_TASA AS TAS_IVA,
        contfra_impu3.TOT_IMPU AS TOT_IVAP,
        transp.*, fracci_totdta.*

        FROM SAAIO_PEDIME pedime
        
        LEFT JOIN SAAIO_FRACCI fracci
        ON pedime.NUM_REFE = fracci.NUM_REFE
        
        LEFT JOIN CTRAC_CLIENT info
        ON pedime.CVE_IMPO = info.CVE_IMP

        LEFT JOIN SAAIO_TRANSP transp
        ON pedime.NUM_REFE = transp.NUM_REFE

        LEFT JOIN SAAIO_CONTFRA contfra_impu6
        ON pedime.NUM_REFE = contfra_impu6.NUM_REFE
        AND fracci.NUM_PART = contfra_impu6.NUM_PART
        AND contfra_impu6.CVE_IMPU = '6'

        LEFT JOIN SAAIO_CONTFRA contfra_impu3
        ON pedime.NUM_REFE = contfra_impu3.NUM_REFE
        AND fracci.NUM_PART = contfra_impu3.NUM_PART
        AND contfra_impu3.CVE_IMPU = '3'

        LEFT JOIN (SELECT NUM_REFE, SUM(MON_DTAP) AS TOT_DTA FROM SAAIO_FRACCI
            GROUP BY NUM_REFE) fracci_totdta
        ON pedime.NUM_REFE = fracci_totdta.NUM_REFE

        LEFT JOIN (SELECT NUM_REFE, CVE_IMPU, SUM(TOT_IMPU) AS TOT_IVA FROM SAAIO_CONTFRA
            WHERE CVE_IMPU = 3 GROUP BY NUM_REFE, CVE_IMPU) contfra_iva
        ON pedime.NUM_REFE = contfra_iva.NUM_REFE

        LEFT JOIN (SELECT NUM_REFE, CVE_IMPU, SUM(TOT_IMPU) AS TOT_ADV FROM SAAIO_CONTFRA
            WHERE CVE_IMPU = 6 GROUP BY NUM_REFE, CVE_IMPU) contfra_adval
        ON pedime.NUM_REFE = contfra_adval.NUM_REFE

        LEFT JOIN (SELECT NUM_REFE, CVE_IMPU, SUM(TOT_IMPU) AS CONTPED_OTRO FROM SAAIO_CONTPED
            WHERE CVE_IMPU = 15 GROUP BY NUM_REFE, CVE_IMPU) contped_otro
        ON pedime.NUM_REFE = contped_otro.NUM_REFE

        WHERE {$where}
        ORDER BY fracci.NUM_PART ASC, pedime.FEC_ENTR DESC";

        $this->operaciones = $db->query($query)->result(true);

        $data = array( 'rows' => array(), 'total' => 0 );

        if (count($this->operaciones) < 1) {
            exit(json_encode($data));
        }

        foreach ($this->operaciones as &$op) {
            $op['NUM_CONT'] = $op['NUM_SELLOS'] = 
            $op['NUM_FACT'] = $op['FEC_FACT'] = 
            $op['NOM_PRO'] = array();

            if (in_array("'{$op['NUM_REFE']}'", $this->referencias)) {
                continue;
            }

            $this->referencias[] = "'{$op['NUM_REFE']}'";
            $this->referencias[] = "' {$op['NUM_REFE']}'";
        }

        $this->referencias = implode(', ', $this->referencias);

        $facturas = $this->factur();
        $contenedores = $this->conten();
        
        foreach ($this->operaciones as &$op) {
            foreach ($facturas as $factur) {
                if (trim($op['NUM_REFE']) === trim($factur['NUM_REFE'])) {
                    if (!in_array($factur['NOM_PRO'], $op['NOM_PRO']) && $factur['NOM_PRO']) {
                        $op['NOM_PRO'][] = $factur['NOM_PRO'];
                    }

                    if (!in_array($factur['NUM_FACT'], $op['NUM_FACT']) && $factur['NUM_FACT']) {
                        $op['NUM_FACT'][] = $factur['NUM_FACT'];
                    }

                    $fecha_factura = trim(str_replace("00:00:00", '', $factur['FEC_FACT']));

                    if (!in_array($fecha_factura, $op['FEC_FACT']) && $fecha_factura) {
                        $op['FEC_FACT'][] = $fecha_factura;
                    }
                }
            }

            foreach ($contenedores as $conten) {
                if (trim($op['NUM_REFE']) === trim($conten['NUM_REFE'])) {
                    if (!in_array($conten['NUM_CONT'], $op['NUM_CONT']) && $conten['NUM_CONT']) {
                        $op['NUM_CONT'][] = $conten['NUM_CONT'];
                    }

                    if (!in_array($conten['NUM_SELLOS'], $op['NUM_SELLOS']) && $conten['NUM_SELLOS']) {
                        $op['NUM_SELLOS'][] = $conten['NUM_SELLOS'];
                    }
                }
            }
        }

        $data['rows'] = $this->operaciones;
        $data['total'] = count($this->operaciones);

        exit(json_encode($data));
    }

    // Obtener numeros de contenedor
    private function conten()
    {
        $db = $this->load->database('fbird', true);

        $query = "SELECT * FROM SAAIO_CONTEN
        
        WHERE NUM_REFE IN ({$this->referencias})";

        return $db->query($query)->result(true);
    }

     // Obtener numeros de factura
     private function factur()
     {
         $db = $this->load->database('fbird', true);
 
         $query = "SELECT factur.*, proved.* FROM SAAIO_FACTUR factur
         
         LEFT JOIN CTRAC_PROVED proved ON factur.CVE_PROV = proved.CVE_PRO
         
         WHERE NUM_REFE IN ({$this->referencias})";
 
         return $db->query($query)->result(true);
     }

    private function setDates()
    {
        // almacenar fecha de inicio
        $date_one = $this->input->get('date_one');

        // almacenar fecha 2, rango de fecha
        $date_two = $this->input->get('date_two');

        // ferificar que la fecha uno se encuentre definida
        // Almacenar fecha dos, rango de fecha
        if (!$date_one) {
            $date_one = date('Y-m-d', time());
        } else {          
            $date_one = date('Y-m-d',strtotime($date_one));            
        }

        // Almacenar fecha dos, rango de fecha
        if (!$date_two) {
            $date_two = date('Y-m-d',strtotime($date_one. "+1 days"));
        } else {          
            $date_two = date('Y-m-d',strtotime($date_two));            
        }

        $this->dateOne = $date_one;
        $this->dateTwo = $date_two;

        return true;
    }
}
