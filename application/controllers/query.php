<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Query extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $db = $this->load->database('fbird', true);

        $result = $db->query($_GET['q'])->result(true);

        $json = json_encode($result);

        exit($json);
    }
}
