<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * PANEL - controlador principal
 * 
 * Para funciones generalizadas del sistema
 *
 * @author  Hecotr Saenz | hectors.digital@gmail.com
 * @version 0.1
 */

class Panel extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
    }

    public function getMasterKey()
    {
        exit(user('mk'));
    }


    // cargar una vista | pagina principal o pagina de inicio de sesion
	public function index()
	{   
        // verificar la vista que se debera cargar
        $view = $this->session->userdata(LOGIN) ? 'main' : 'login';

        // cargar vista
		$this->load->view($view);

        /*
         Verificando si existe la variable de sesion LOGIN, se debera 
         cargar la vista main o login
         - main: Pagina principal.
         - login: Pagina de inicio de sesion.
        */
	}

    public function getLog()
    {
        $rows = array();
        
        if (!file_exists("static\\users_log.json")) {
            try {
                $fopen = fopen("static\\users_log.json", "w");
                fwrite($fopen, json_encode(array()));
                fclose($fopen);
            }
            catch (exception $ex) {}
        }
        
        $data = file_get_contents("static\\users_log.json");

        $data = json_decode($data);
        
        exit(json_encode(array(
            'rows' => $data,
            'total' => count($data)
        )));
    }
}
