<?php if ( ! defined('BASEPATH')) exit("No direct script access allowed");

class Logout extends CI_Controller {

	public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata(LOGIN)) $this->session->unset_userdata(LOGIN);

		header('Location:' . base_url());
    }

	public function index() {}
}

/* End of file logout.php */
/* Location: ./application/controllers/logout.php */