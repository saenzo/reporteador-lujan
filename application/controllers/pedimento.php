<?php if ( ! defined('BASEPATH')) exit("No direct script access allowed");

class Pedimento extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	// buscar por un rango de fechas
    public function bydate()
    {
        // almacenar fecha de inicio
        $date_one = $this->input->post('date_one');

        // almacenar fecha 2, rango de fecha
        $date_two = $this->input->post('date_two');

        // ferificar que la fecha uno se encuentre definida
        if ( ! $date_one ) exit(0);

        // almacenar fecha de inicio
        $date_one = date('Y-m-d',strtotime($date_one));

        // almacenar fecha dos, rango de fecha
        if (!$date_two) {
            $date_two = date('Y-m-d',strtotime($date_one. "+1 days"));
        } else {          
            $date_two = date('Y-m-d',strtotime($date_two));            
        }

        // realizar consulta
        $e = $this->dbByDate($date_one, $date_two);
        
        // verificar consulta
        if ( count($e) < 1 ) {
            exit(0);
        }
        
        $this->load->model('pedimento_dom');

        $e = $this->pedimento_dom->getTable($e);

        $res = json_encode($e);

        exit($res);
        
    }

    // buscar por una referencia 
    public function byref()
    {       
        // almacenar la referencia
        $ref = $this->input->post('ref');

        // verificar
        if ( ! $ref ) exit(0);

        // limpiar referencia
        $ref = strtoupper(trim($ref));
       
        $e = $this->dbByRef($ref);   

        // verificar consulta
        if ( count($e) <= 0 ) exit(0);
        
        $this->load->model('pedimento_dom');

        $e = $this->pedimento_dom->getTable($e);

        $res = json_encode($e);

        exit($res);
       
    }

    function toExcelByDate()
    {
        // almacenar fecha de inicio
        $date_one = $this->input->post('date_one');

        // almacenar fecha 2, rango de fecha
        $date_two = $this->input->post('date_two');

        // ferificar que la fecha uno se encuentre definida
        if ( ! $date_one ) exit(0);

        // almacenar fecha de inicio
        $date_one = date('Y-m-d',strtotime($date_one));

        // almacenar fecha dos, rango de fecha
        if ( ! $date_two ) {
            
            $date_two = date('Y-m-d',strtotime($date_one. "+1 days"));
        
        } else {          
            
            $date_two = date('Y-m-d',strtotime($date_two));            
        
        }

        // realizar consulta
        $e = $this->dbByDate($date_one, $date_two);

        // verificar consulta
        if ( count($e) <= 0 ) exit(0);
     
        $this->excel($e, 'pedimento.xls', 'pedimento');
       
    }

    public function toExcelByRef()
    {   
        // almacenar la referencia
        $ref = $this->input->post('ref');

        // verificar
        if ( ! $ref ) exit(0);

        // limpiar referencia
        $ref = strtoupper(trim($ref));
       
        $e = $this->dbByRef($ref);   

        // verificar consulta
        if ( count($e) <= 0 ) exit(0);

        $this->excel($e, 'pedimento.xls', 'pedimento');
    }

    // ejecutar una consulta utilizando un rango de fechas
    public function dbByDate($date_one, $date_two)
    {	
    	$db = $this->load->database('fbird', true);

        // preparar consulta
        if ( in_array( 'admin', user('client') ) ) {
        	$where = "FEC_ENTR BETWEEN '{$date_one}' AND '{$date_two}'";
        } else {
            // obtener clientes a los que puede accesar el usuario
            $keys = implode( ',', user('other') );
            
            $where = "CVE_IMPO IN ({$keys}) AND FEC_ENTR BETWEEN '{$date_one}' AND '{$date_two}'";
        }

        $query = $this->query($where);

        return $db->query($query)->result(true);
    }

    // ejecutar una consulta utilizando una referencia
    public function dbByRef($ref)
    {   
        $db = $this->load->database('fbird', true);

        // verificar el campo que se utilizara
        $field = ( strlen($ref) == 7 && is_numeric($ref) ) ? 'NUM_PEDI' : 'NUM_REFE';

        // preparar consulta
        if ( in_array('admin', user('client')) ) {

            $where = "{$field} = '{$ref}'";
        
        } else {

            // obtener clientes a los que puede accesar el usuario
            $keys = implode( ',', user('other') );

            $where = "CVE_IMPO IN ({$keys}) AND {$field} = '{$ref}'";
        }

        $query = $this->query($where);

        return $db->query($query)->result(true);
    }


    public function query($where)
    {
    	return "SELECT 
                NUM_REFE,
                CVE_IMPO,
                IMP_EXPO,
                TIP_PEDI,
                ADU_DESP,
                PAT_AGEN,
                NUM_PEDI,
                ADU_ENTR,
                FEC_ENTR,
                TIP_CAMB,
                CVE_PEDI,
                REG_ADUA,
                DES_ORIG,
                MAR_NUME,
                PES_BRUT,
                MTR_ENTR,
                MTR_ARRI,
                MTR_SALI,
                TIP_MOVA,
                VAL_DLLS,
                VAL_COME,
                TOT_INCR,
                TOT_DEDU,
                VAL_NORM,
                FAC_AJUS,
                FEC_PAGO,
                PAR_2DTA,
                FAC_ACTU,
                NUM_FRAC,
                TOT_EFEC,
                TOT_OTRO,
                FIR_ELEC,
                NUM_CAND,
                TOT_VEHI,
                CVE_BANC,
                NUM_CAJA,
                DIA_PAGO,
                HOR_PAGO,
                TOT_PAGO,
                CAN_BULT,
                FIR_PAGO,
                NUM_OPER FROM SAAIO_PEDIME WHERE {$where} ORDER BY FEC_ENTR DESC
            ";
    }

    public function excel($e, $filename, $shet)
    {   
        // cargar creador de archivos excel
        $this->load->library('excel');

        // establecer propiedades
        $this->excel->getProperties()->setCreator("Sergio Lujan");
        $this->excel->getProperties()->setLastModifiedBy("Sergio Lujan");
        $this->excel->getProperties()->setTitle("Office 2007 XLSX Document");
        $this->excel->getProperties()->setSubject("Office 2007 XLSX Document");
        $this->excel->getProperties()->setDescription("document for Office 2007 XLSX, generated by Sergio Lujan");

        // generar datos
        $this->excel->setActiveSheetIndex(0);

        // columnas
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(2).'2', 'referencia');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(3).'2', 'clave cliente');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(4).'2', 'tipo operacion');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(5).'2', 'tipo pedimento');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(6).'2', 'aduana de despacho');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(7).'2', 'patente');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(8).'2', 'numero de pedimento');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(9).'2', 'aduana de entrada');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(10).'2', 'fecha de entrada');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(11).'2', 'tipo de cambio');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(12).'2', 'clave pedimento');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(13).'2', 'regimen');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(14).'2', 'destino de la mercancia');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(15).'2', 'marcas (no comerciales)');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(16).'2', 'peso bruto');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(17).'2', 'medio de transporte entrada');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(18).'2', 'medio de transporte arribo');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(19).'2', 'medio de transporte salida');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(20).'2', 'tipo de moneda');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(21).'2', 'valor en dolares');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(22).'2', 'valor comercial');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(23).'2', 'incrementables');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(24).'2', 'deducibles');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(25).'2', 'valor normal');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(26).'2', 'factor ajuste');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(27).'2', 'fecha de pago');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(28).'2', 'dta (derecho tramite aduanero)');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(29).'2', 'factor actualizacion');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(30).'2', 'fraciones');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(31).'2', 'total efectivo');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(32).'2', 'total otro');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(33).'2', 'firma electronica validacion');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(34).'2', 'candados');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(35).'2', 'total vahiculos');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(36).'2', 'clave de banco');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(37).'2', 'numero de caja banco');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(38).'2', 'fecha de pago');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(39).'2', 'hora de pago');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(40).'2', 'pago total');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(41).'2', 'cantidad de bultos');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(42).'2', 'firma de pago');
        $this->excel->getActiveSheet()->SetCellValue((string) num2alpha(43).'2', 'numero de operacion');
            
        // renglones
        $row = 3; 

        foreach ($e as $values) {

            $n = 2; // columna

            // preparar el resto de los valores
            foreach ($values as $i => $val) {

                // columna + numero de renglon
                $c = (string) num2alpha($n) . $row;

                $this->excel->getActiveSheet()->SetCellValue($c, $val);

                $n++;
                    
            }

            $row++;
        }       

        // titulo de la hoja
        $this->excel->getActiveSheet()->setTitle($shet);
                
        // Cuardar como archivo Excel 2007
        $objWriter = new PHPExcel_Writer_Excel2007($this->excel);
        $objWriter->save(str_replace('.php', '.xls', __FILE__));

        // cargar helper de archivos
        $this->load->helper('file');

        ob_clean();
        header('Content-Type:' . get_mime_by_extension($filename));
        header('Content-Disposition: attachment; filename="'. $filename. '"');
        header('Cache-Control: max-age=0');
        ob_flush();

        $objWriter->save('php://output');
    }

    // obtener informacion sobre una referencia
    public function info()
    {   
        // almacenar referencia
        $ref = $this->input->post('ref');

        $imp = $this->input->post('imp');

        // verificar
        if ( ! $ref || ! $imp ) exit(0);

        // limpiar referencia
        $ref = strtoupper(trim($ref));
       
        $x = $this->dbByRef($ref);

        if (count($x) >= 1) {
            $x = $x[0];
        } else {
            $x = array();
        }

        // limpiar referencia imp
        $imp = strtoupper(trim($imp));

        $cols = 'NOM_IMP, DIR_IMP, POB_IMP, CP_IMP, RFC_IMP, CUR_IMP, NOI_IMP, NOE_IMP, EFE_IMP, PAI_IMP';

        $q = "SELECT {$cols} FROM CTRAC_CLIENT WHERE CVE_IMP = '{$imp}'";

        $t = $this->load->database('fbird', true)->query($q)->result(true);

        if (count($t) >= 1) {
            $t = $t[0];
        }

        $x += $t;

        $x['FEC_ENTR'] = date('d/m/Y',strtotime($x['FEC_ENTR']));

        $x['FEC_PAGO'] = date('d/m/Y',strtotime($x['FEC_PAGO']));

        exit(json_encode($x));
    }
}
