<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contenedor extends CI_Controller
{
 
	public function __construct()
    {
        parent::__construct();
    }

    // Obtener tabla cuenta gastos americana
    public function index()
    {
        $db = $this->load->database('fbird', true);

        $num_refe = $_GET['num_refe'];

        $query = "SELECT * FROM SAAIO_CONTEN
        WHERE NUM_REFE = '{$num_refe}' OR NUM_REFE = ' {$num_refe}'";

        $result = $db->query($query)->result(true);

        $data = array(
            'rows' => $result,
            'total' => count($result)
        );

        exit(json_encode($data));
    }
}
