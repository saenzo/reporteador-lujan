<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Facturas extends CI_Controller
{
 
	public function __construct()
    {
        parent::__construct();
    }

    // Obtener tabla cuenta gastos americana
    public function index()
    {
        $db = $this->load->database('fbird', true);

        $num_refe = $_GET['num_refe'];

        $query = "SELECT factur.*, proved.* 
        
        FROM SAAIO_FACTUR factur
        
        LEFT JOIN CTRAC_PROVED proved
        ON factur.CVE_PROV = proved.CVE_PRO

        WHERE factur.NUM_REFE = '{$num_refe}'
        OR factur.NUM_REFE = ' {$num_refe}'";

        $result = $db->query($query)->result(true);
        
        $data = array(
            'rows' => $result,
            'total' => count($result)
        );

        exit(json_encode($data));
    }
}
