<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, cgm, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;

// Firebird
$db['fbird']['hostname'] = '10.13.18.109';
$db['fbird']['username'] = 'SYSDBA';
$db['fbird']['password'] = 'masterkey';
$db['fbird']['database'] = 'f:\CASAWIN\CSAAIWIN\DATOS\CASA.GDB';
$db['fbird']['dbdriver'] = 'firebird';
$db['fbird']['dbprefix'] = '';
$db['fbird']['pconnect'] = FALSE;    
$db['fbird']['db_debug'] = TRUE;
$db['fbird']['cache_on'] = FALSE;
$db['fbird']['cachedir'] = '';
$db['fbird']['char_set'] = 'utf8';
$db['fbird']['dbcollat'] = 'utf8_general_ci';
$db['fbird']['swap_pre'] = '';
$db['fbird']['autoinit'] = TRUE;
$db['fbird']['stricton'] = FALSE;


if (gethostname() !== 'ServerReportes') {
	$db['cgm']['hostname'] = 'DSN=cgm;Database=Vial;';
	$db['cga']['hostname'] = 'DSN=cga;Database=Vial;';
	$db['cta']['hostname'] = 'DSN=cga;Database=Vial;';
} else {
	$db['cgm']['hostname'] = 'Driver={SQL Server};Database=Vial;Server=10.13.18.100;Uid=sistemasvial;Pwd=VialAdmin;';
	$db['cga']['hostname'] = 'Driver={SQL Server};Database=Vial;Server=10.13.18.100;Uid=sistemasvial;Pwd=VialAdmin;';
	$db['cta']['hostname'] = 'Driver={SQL Server};Database=Vial;Server=10.13.18.100;Uid=sistemasvial;Pwd=VialAdmin;';
}

// Cuentas de gastos mexicana
$db['cgm']['dbdriver'] = 'odbc';
$db['cgm']['dbprefix'] = '';
$db['cgm']['pconnect'] = FALSE;    
$db['cgm']['db_debug'] = TRUE;
$db['cgm']['cache_on'] = FALSE;
$db['cgm']['cachedir'] = '';
$db['cgm']['char_set'] = 'utf8';
$db['cgm']['dbcollat'] = 'utf8_general_ci';
$db['cgm']['swap_pre'] = '';
$db['cgm']['autoinit'] = TRUE;
$db['cgm']['stricton'] = FALSE;

// Cuentas de gastos americana
$db['cga']['dbdriver'] = 'odbc';
$db['cga']['dbprefix'] = '';
$db['cga']['pconnect'] = FALSE;    
$db['cga']['db_debug'] = TRUE;
$db['cga']['cache_on'] = FALSE;
$db['cga']['cachedir'] = '';
$db['cga']['char_set'] = 'utf8';
$db['cga']['dbcollat'] = 'utf8_general_ci';
$db['cga']['swap_pre'] = '';
$db['cga']['autoinit'] = TRUE;
$db['cga']['stricton'] = FALSE;

// Cuentas de gastos americana
$db['cta']['dbdriver'] = 'odbc';
$db['cta']['dbprefix'] = '';
$db['cta']['pconnect'] = FALSE;    
$db['cta']['db_debug'] = TRUE;
$db['cta']['cache_on'] = FALSE;
$db['cta']['cachedir'] = '';
$db['cta']['char_set'] = 'utf8';
$db['cta']['dbcollat'] = 'utf8_general_ci';
$db['cta']['swap_pre'] = '';
$db['cta']['autoinit'] = TRUE;
$db['cta']['stricton'] = FALSE;
