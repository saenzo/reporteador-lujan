<?php if ( ! defined('BASEPATH')) exit("No direct script access allowed");

// creador de elementos _html
class Dom {
	// almacenar una instancia de _CI
	var $ci;

	public function __construct()
	{   
		// obtener instancia de CI
		$this->ci =& get_instance();

    }

	// preparar fracciones para mostrar
	public function getFracc($e)
	{
		$content = array();
		
		foreach ($e as $value) {
			$content[] = "
				<div class='panel panel-primary'>                           
                	<div class='panel-heading text-center'>
                    	FRACCIONES
                	</div>
                    <div class='panel-body'>
                    	<div class='col-md-6 col-sm-6 col-lg-6 col-xs-6'>
                    		<label>Referencia: </label>
                    		{$value['NUM_REFE']}<br><br>

                    		<label>Fraccion: </label>
                    		{$value['FRACCION']}<br>
                    		
                    		<label>Monto factura: </label>
                    		{$value['MON_FACT']}<br>
                    		<label>Valor normal: </label>
                    		{$value['VAL_NORF']}<br><br>

                    		<label>Cantidad tarifa: </label>
                    		{$value['CAN_TARI']}<br>
                    		<label>Unidad tarifa: </label>
                    		{$value['UNI_TARI']}<br>
                    		<label>Pais origen: </label>
                    		{$value['PAI_ORIG']}<br>
                    	</div>
                    	<div class='col-md-6 col-sm-6 col-lg-6 col-xs-6'>
                    		<label>Consecutivo: </label>
                    		{$value['NUM_PART']}<br><br>

                    		<label>Tipo de moneda: </label>
                    		{$value['TIP_MONE']}<br>
                    		<label>Valor comercial: </label>
                    		{$value['VAL_COMF']}<br><br>

                    		<label>Cantidad comercial: </label>
                    		{$value['CAN_FACT']}<br>
                    		<label>Unidad comercial: </label>
                    		{$value['UNI_FACT']}<br>
                    		<label>Pais vendedor: </label>
                    		{$value['PAI_VEND']}<br>
                    	</div>
                    </div>
                    <div class='panel-heading text-center'>
                    	TASAS, IMPUESTOS
                	</div>
                    <div class='panel-body'>
                    	<div class='col-md-4 col-sm-4 col-lg-4 col-xs-4'>
                    		<label>Valoracion: </label>
                    		{$value['CVE_VALO']}<br><br>

                    		<label>Advalorem: </label>
                    		{$value['ADVAL']}<br>
                    		<label>IVA: </label>
                    		{$value['PORC_IVA']}<br>
                    		<label>Cuota compensatoria: </label>
                    		{$value['MON_COM1']}<br>
                    	</div>
                    	<div class='col-md-4 col-sm-4 col-lg-4 col-xs-4'>
                    		<br><br>

                    		<label>TLC: </label>
                    		{$value['CAS_TLCS']}<br>
                    		<label>IEPS: </label>
                    		{$value['MON_IEPS']}<br>
                    		<label>Valor agregado: </label>
                    		{$value['VAL_AGRE']}<br>
                    	</div>
                    	<div class='col-md-4 col-sm-4 col-lg-4 col-xs-4'>
                    		<label>Vinculacion: </label>
                    		{$value['CVE_VINC']}<br>
                    		<label>Pais TLC: </label>
                    		{$value['COM_TLC']}<br>
                    		<label>DTA</label>
                    		{$value['MON_DTAP']}<br>
                    		
                    	</div>
                    </div>
                    <div class='panel-heading text-center'>
                    	MARCAS, MODELOS, OBSERVACIONES
                	</div>
                    <div class='panel-body'>
                    	<div class='col-md-6 col-sm-6 col-lg-6 col-xs-6'>
                    		<label>Marcas: </label>
                    		{$value['MAR_MERC']}<br>
                    		<label>Modelo: </label>
                    		{$value['MOD_MERC']}<br><br>

                    		
                    	</div>
                    	<div class='col-md-6 col-sm-6 col-lg-6 col-xs-6'>
                    		<br><br>

                    		<label>Codigo producto: </label>
                    		{$value['COD_PROD']}<br>
                    	</div>
                    </div>
                </div>
                <hr>
			";
		}

		return $content;
		
	}
}

/* fin del archivo dom.php */
/* ubicacion: ./aplication/libraries/dom.php */