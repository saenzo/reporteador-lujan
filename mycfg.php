<?php
/**
 *
 *---------------------------------------------------------------
 * CONTROL DE CABECERAS.
 *---------------------------------------------------------------
 *
 */
	// no utilizar cache
	header('Cache-Control: no-cache');
	header('Pragma: no-cache');

	// codificacion de caracteres UTF-8
	header('Content-Type: text/html; charset=utf-8');
/**
 *
 *---------------------------------------------------------------
 * ESTABLECIMIENTO DE LA ZONA HORARIA
 *---------------------------------------------------------------
 *
 */
	ini_set('memory_limit', '-1');
	// definicion de la zona horaria
	date_default_timezone_set('America/Monterrey');
/**
 *
 *---------------------------------------------------------------
 * CONFIGURACION INICIAL
 *---------------------------------------------------------------
 *
 */
	// almacenar configuracion inicial
	$mycfg = array();

	// contenedor de la aplicacion
	$mycfg['basename'] = "";

	// version actual del sistema
	$mycfg['version'] = "v1.0.1 | 15/03/2019";

	// contenedor de archivos externos dentro de la aplicacion
	$mycfg['rspath'] = "static";

	if ($_SERVER['SERVER_NAME'] === 'reporteador.local') {
		// URL de la aplicacion
		$mycfg['baseurl'] = "http://{$_SERVER['SERVER_NAME']}/{$mycfg['basename']}";
	} else {
		// URL de la aplicacion
		$mycfg['baseurl'] = "http://{$_SERVER['SERVER_NAME']}:8080/{$mycfg['basename']}";
	}
	

	// ruta completa de los archivos externos de la aplicacion
	$mycfg['rspath'] = "{$mycfg['baseurl']}{$mycfg['rspath']}/";
/**
 *
 *---------------------------------------------------------------
 * REQUISITOS DEL SISTEMA
 *---------------------------------------------------------------
 *
 */
	// verificar modo escritura
	/*if (! in_array('mod_rewrite', apache_get_modules())) {
		// salir y mostrar mensaje de error
		exit("El modulo escritura en el servidor no esta encendido");
	}*/

	// verificar la extension de firebird para PHP
	if (! extension_loaded('interbase')) {
    	exit('La extension interbase(firebird) no se cargo');
	}
/**
 *
 *---------------------------------------------------------------
 * CONTORL DE VOLUMEN DE ARCHIVOS
 *---------------------------------------------------------------
 *
 */
	//limites y carga de archivos
	/*ini_set('memory_limit', '-1');
	ini_set('upload_max_filesize', '10G');
	ini_set('post_max_size', '10G');
	ini_set('max_input_time', 10080);
	ini_set('max_execution_time', 10080);*/
/**
 *
 *---------------------------------------------------------------
 * DEFINICION DE CONSTANTES
 *---------------------------------------------------------------
 *
 */
	// definir version actual
	define('VERSION', $mycfg['version']);

	// url base
	define('BASEURL', $mycfg['baseurl']);

	// definir ubicacion de los archivos externos
	define('RSPATH', $mycfg['rspath']);